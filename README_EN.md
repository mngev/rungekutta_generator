# Runge–Kutta methods generation (Julia Language code)

`Python 3` scripts for classical and embedded Runge-Kutta methods code
generation. The program generates [Julia language](https://julialang.org/)
code. Special class `ButcherTable` is implemented. It is possible to
 create `ButcherTable` object from python dictionary (or list of
 dictionaries), JSON file, or constrict it explicitly. The list of Runge--Kutta
 methods is available in `./tables_json` dir in JSON format.

The generated codes can be used for educational and research purposes.
For the purposes of high performance computing better to use a more
optimized and reliable codes, such as:

- [Fortran and Matlab Codes from Hairer and al.](http://www.unige.ch/~hairer/software.html)
- [Dopri.jl is a Julia wrapper for the DOPRI5 and DOP853 integrators](https://github.com/helgee/Dopri.jl)
- [Julia DiffEq Lib](https://github.com/JuliaDiffEq)
- [SciPy ODE solvers](https://docs.scipy.org/doc/scipy-0.19.0/reference/generated/scipy.integrate.ode.html)

## Code generation

We use [Jinja2](http://jinja.pocoo.org/) template engine for code generation.
The implementation of the generic algorithm for the method of Runge-Kutta method,
which could use any table Butcher in his work, leads to non-optimal use
of memory and computational resources. The coefficients of the butcher table
has to be stored in the array that is not optimal, as most of them are zero.
A common practice is to declare the coefficients of the methods Runge-Kutta
as constant.

And that makes our code generator automatically. The result of it's work
are the classical Runge-Kutta methods with constant step and embedded methods
with a step size control. Methods with dense output or order switching
are not implemented. Such methods are relatively few in number, and it's
better to code them manually.

To run the generator one need to run `rk_generator.py` or `erk_generator.py`
script. The path to algorithms template and JSON files with Butcher tables
are in `settings.py` file.

## Butcher table info in JSON file format

The file structure is quite obvious. It consists of a list of JSON
objects, each of which contains information required to build the
Runge--Kutta method, such as `name`, `description` (strings); `stage`,
`order` (integers); `a`, `b`, `c` (arrays of floats or strings). The
only non-obvious point is that if the coefficients are rational function,
then they need to be set as a string of the form `p/q` (e.g. `23/25`).
They will be implicitly converted to `numpy` array with
[`Fraction`](https://docs.python.org/3.6/library/fractions.html) type
 elements. For embedded Runge-Kutta method a few additional fields
 required: `extrapolation_order` (integer) and `b_hat` (array of floats
 or strings).

**Example:**

 ```json
 {
  "name": "ERK23",
  "description": " Fehlberg, 2(3)A 1968
  "stage": 3,
  "order": 2,
  "extrapolation_order": "3",
  "a": [
      ["0", "0", "0"],
      ["1", "0", "0"],
      ["1/4", "1/4", "0"]
  ],
  "b": ["1/2", "1/2", "0"],
  "b_hat": ["1/6", "1/6", "2/3"],
  "c": ["0", "1", "1/2"]
  }
 ```

## Templates variables

All templates are in `./template` dir. For classical Runge--Kutta method
there are following templates.

- `rk_method.jl`
    - `function_name` the name of the current Runge--Kutta schema.
      Type is `str`.
    - `only_last` if `false` will turn off pieces of code that save
      intermediate `t` and  `x` values in arrays for function return.
      If `true` it will generate the function that return `t_n` and
      `x_n ≈ x(t_n)`. Type is `bool`.
    - `a`, `b`, `c` Runge--Kutta coefficients.
- `rk_doc.md` the function's doc string; combines documentation for the
  function with the argument `last` and for the function without it.
  The only variable is `function_name`.
- `rk_module.jl` the template with "magic" Jinja2 `{% include %}` tag
  that automatically loads file `rk.jl` with a full list of generated
  functions. Content of this file is framed by the words `module` `end`.
  The variable `methods_list` contains the names of all the numerical
  schemes which are generated. It shell be list of strings and
  will be parsed in array of symbols (Julia `Symbol` type).

Templates for embedded Runge--Kutta are mostly the same: `erk_method.jl`,
`erk_doc.md` and `erk_module.jl`.