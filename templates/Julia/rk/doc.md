Runge-Kutta method with constant step size.

The function has two methods:
```
{{function_name}}(func, x_0, h, t_start, t_stop) -> (t, x)
{{function_name}}(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.