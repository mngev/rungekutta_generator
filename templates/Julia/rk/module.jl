module RK

functions = [
{%- for f in methods_list -%}
  {%- if not loop.first -%}, {%- endif -%}
  :{{f}}
{%- endfor -%}]

{{source_code}}

end # module RK
