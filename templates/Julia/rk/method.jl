function {{function_name}}(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64
  {%- if only_last -%}
  , last::Bool
  {%- endif -%})
  {%- if only_last -%}
    ::Tuple{Float64,Vector{Float64}}
  {%- else -%}
    ::Tuple{Vector{Float64},Matrix{Float64}}
  {%- endif %}

  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  {% if not only_last %}
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  {% endif %}
  
  {% for i in range(stage) %}
    {% for j in range(stage) if j < i and a[i,j] != 0 %}
  local a{{i+1}}{{j+1}} = {{a[i,j]}}
    {% endfor %}
  {% endfor %}
  
  {% for i in range(stage) if b[i] != 0 %}
  local b{{i+1}} = {{b[i]}}
  {% endfor %}
  
  {% for i in range(stage) if c[i] != 0 %}
  local c{{i+1}} = {{c[i]}}
  {% endfor %}
  
  {% for i in range(stage) %}
  local k{{i+1}}::Vector{Float64} = zeros(Float64, EQN)
  {% endfor %}
  
  while true
    {% if not only_last %}
    push!(X, x)
    push!(T, t)
    {% endif %}
    
    {% for i in range(stage) %}
    k{{i+1}} = func(t{% if c[i] != 0 %} + h*c{{i+1}}{% endif %}, x{% if i > 0 %} + h*({% endif %}
      {%- for j in range(stage) if j < i and a[i,j] != 0 -%}
        k{{j+1}}*a{{i+1}}{{j+1}}{% if not loop.last %} + {% endif %}
      {% endfor -%}{% if i > 0 %}){% endif %})
    {% endfor %}
    
    x = x + h*({% for j in range(stage) if b[j] != 0 %}{% if not loop.first %} + {% endif %}b{{j+1}}*k{{j+1}}{% endfor %})
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  {% if only_last %}
  return (t, x)
  {% else %}
  return(T, transpose(hcat(X...)))
  {% endif %}
  
end # {{function_name}}