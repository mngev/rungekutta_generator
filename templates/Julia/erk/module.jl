module ERK

include("../StepControl.jl")

functions = [
{%- for f in methods_list -%}
  {%- if not loop.first -%}, {%- endif -%}
  :{{f}}
{%- endfor -%}]

info_functions = [
{%- for f in methods_list -%}
  {%- if not loop.first -%}, {%- endif -%}
  :{{f}}_info
{%- endfor -%}]

include("erk_base.jl")
include("erk_info.jl")


end # module ERK
