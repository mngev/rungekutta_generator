{% if not with_info %}
Embedded Runge--Kutta with base method's order {{order}}, 
extrapolation order {{eoder}} and numbers of stages {{stage}}.
With step control and without dense output.

```
{{function_name}}(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
{{function_name}}(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    
{% else %}
Collects information (accepted steps, rejected steps etc.) about embedded 
Runge--Kutta with base method's order {{order}}, 
extrapolation order {{eoder}} and numbers of stages {{stage}}.
With step control and without dense output.

```
{{function_name}}_info(func, A_tol, R_tol, x_0, start, stop) -> 
                              (accepted_t, accepted_h, rejected_t, rejected_h)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;

# Return values

* `accepted_t::Vector{Float64}`: all time points where step was accepted;
* `accepted_h::Vector{Float64}`: all accepted step sizes;
* `rejected_t::Vector{Float64}`: all time points where step was rejected;
* `rejected_h::Vector{Float64}`: all rejected step sizes;
*     `errors::Vector{Float64}`: inner error values.
{% endif %}