function {{function_name}}
  {%- if with_info -%}
  _info
  {%- endif -%}
  (func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64
  {%- if only_last -%}
  , last::Bool
  {%- endif -%})
  {%- if only_last -%}
    ::Tuple{Float64,Vector{Float64}}
  {%- elif with_info -%}
    ::Tuple{Vector{Float64}, Vector{Float64}, Vector{Float64}, Vector{Float64}, Vector{Float64}, Vector{Float64}}
  {%- else -%}
    ::Tuple{Vector{Float64},Matrix{Float64}}
  {%- endif %}

  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  {% for i in range(stage) %}
    {% for j in range(stage) if j < i and a[i,j] != 0 %}
  local a{{i+1}}{{j+1}} = {{'%0.17g'| format(a[i,j]|float)}}
  {#local a{{i+1}}{{j+1}} = {{a[i,j]}}#}
    {% endfor %}
  {% endfor %}

  {% for i in range(stage) if b[i] != 0 %}
  local b{{i+1}} = {{'%0.17g'| format(b[i]|float)}}
  {#local b{{i+1}} = {{b[i]}}#}
  {% endfor %}

  {% for i in range(stage) if b_hat[i] != 0 %}
  local b_hat{{i+1}} = {{'%0.17g'| format(b_hat[i]|float)}}
  {#local b_hat{{i+1}} = {{b_hat[i]}}#}
  {% endfor %}

  {% for i in range(stage) if c[i] != 0 %}
  local c{{i+1}} = {{'%0.17g'| format(c[i]|float)}}
  {#local c{{i+1}} = {{c[i]}}#}
  {% endfor %}
  {#=======================================================================#}
  {% if with_info %}
  accepted_t = Vector{Float64}()
  accepted_h = Vector{Float64}()
  rejected_t = Vector{Float64}()
  rejected_h = Vector{Float64}()
  accepted_err = Vector{Float64}()
  rejected_err = Vector{Float64}()
  {% endif %}
  {#=======================================================================#}

  {% for i in range(stage) %}
  local k{{i+1}}::Vector{Float64} = zeros(Float64, EQN)
  {% endfor %}

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, {{order}}, {{eoder}})

  {% if not only_last and not with_info %}
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)
  {% endif %}

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    {% for i in range(stage) %}
    x = x_0
    {%- if i > 0 %} + h*(
      {%- for j in range(stage) if j < i and a[i,j] != 0 -%}
        k{{j+1}}*a{{i+1}}{{j+1}}{% if not loop.last %} + {% endif %}
      {%- endfor %})
    {%- endif %}

    k{{i+1}} = func(t{% if c[i] != 0 %} + h*c{{i+1}}{% endif %}, x)
    {% endfor %}

    x = x_0 + h*({% for j in range(stage) if b[j] != 0 %}{% if not loop.first %} + {% endif %}b{{j+1}}*k{{j+1}}{% endfor %})
    x_hat = x_0 + h*({% for j in range(stage) if b_hat[j] != 0 %}{% if not loop.first %} + {% endif %}b_hat{{j+1}}*k{{j+1}}{% endfor %})

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      {% if with_info %}
      push!(accepted_t, t)
      push!(accepted_err, err)
      push!(accepted_h, h)
      {% endif %}
      x_0 = x
      t = t + h
      h = h_new
    else
      {% if with_info %}
      push!(rejected_t, t)
      push!(rejected_err, err)
      push!(rejected_h, h)
      {% endif %}
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    {% if not only_last and not with_info%}
    if accepted
      push!(X, x)
      push!(T, t)
    end
    {% endif %}
  end

  {% if only_last %}
  return (t, x)
  {#=======================================================================#}
  {% elif with_info %}
  return (accepted_t, accepted_h, rejected_t, rejected_h, accepted_err, rejected_err)
  {#=======================================================================#}
  {% else %}
  return(T, transpose(hcat(X...)))
  {% endif %}
end # function {{function_name}}
