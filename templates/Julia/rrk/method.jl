function {{function_name}}(func::Function, dfunc::Function, Jfunc::Function, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, A_tol::Float64, R_tol::Float64)::Tuple{Vector{Float64},Matrix{Float64}}

  local EQN = length(x_start)
  
  {% for i in range(stage) %}
    {% for j in range(stage) if j < i and a[i,j] != 0 %}
  local a{{i+1}}{{j+1}} = {{a[i,j]}}
    {% endfor %}
  {% endfor %}
  
  {% for i in range(stage) %}
    {% for j in range(stage) if j <= i and a_hat[i,j] != 0 %}
  local a_hat{{i+1}}{{j+1}} = {{a_hat[i,j]}}
    {% endfor %}
  {% endfor %}
  
  {% for i in range(stage) if b[i] != 0 %}
  local b{{i+1}} = {{b[i]}}
  {% endfor %}
  {% for i in range(stage) if b_hat[i] != 0 %}
  local b_hat{{i+1}} = {{b_hat[i]}}
  {% endfor %}
  
  {% for i in range(stage) if c[i] != 0 %}
  local c{{i+1}} = {{c[i]}}
  {% endfor %}
  {% for i in range(stage) if c_hat[i] != 0 %}
  local c_hat{{i+1}} = {{c_hat[i]}}
  {% endfor %}
  
  {% for i in range(stage) %}
  local k{{i+1}}::Vector{Float64} = zeros(Float64, EQN)
  {% endfor %}
  
  local t::Float64
  local x::Vector{Float64}
  
  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, {{order}}, {{eorder}})

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    df = dfunc(t, x_0)
    J = Jfunc(t, x_0)
    
    A = inv(diagm([1.0 / (h * a_hat11) for _ in 1:EQN]) - J)
    
    {% for i in range(stage) %}
    x = x_0
    {%- if i > 0 %}
      {%- for j in range(stage) if j < i and a[i,j] != 0 -%}
        {% if loop.first %} + {% endif %}k{{j+1}} * a{{i+1}}{{j+1}}{% if not loop.last %} + {% endif %}
      {%- endfor -%}
    {%- endif %}

    k{{i+1}} = func(t{% if c[i] != 0 %} + h*c{{i+1}}{% endif %}, x) {% if i > 0 %}+ ({% endif %}
      {%- for j in range(stage) if j < i and a_hat[i,j] != 0 -%}
        a_hat{{i+1}}{{j+1}} * k{{j+1}}{% if not loop.last %} + {% endif %}
      {% endfor -%}{% if i > 0 %}) / h {% endif %}
      {% if c[i] != 0 %} + c{{i+1}} * h * df{% endif %}

    k{{i+1}} = A * k{{i+1}}
    {% endfor %}
    
    x = x_0 + {% for j in range(stage) if b[j] != 0 %}{% if not loop.first %} + {% endif %}b{{j+1}}*k{{j+1}}{% endfor %}
    
    x_hat = x_0 + {% for j in range(stage) if b_hat[j] != 0 %}{% if not loop.first %} + {% endif %}b_hat{{j+1}}*k{{j+1}}{% endfor %}

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end
  
  return(T, hcat(X...))
  
end # {{function_name}}