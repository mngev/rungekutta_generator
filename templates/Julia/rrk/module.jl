module RRK

include("../StepControl.jl")

functions = [
{%- for f in methods_list -%}
  {%- if not loop.first -%}, {%- endif -%}
  :{{f}}
{%- endfor -%}]

include("rrk_base.jl")

end # module RRK
