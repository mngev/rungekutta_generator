Классический метод Рунге-Кутты с постоянным шагом
    {{function_name}}(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений.
