def {{function_name}}(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """{{docstring}}"""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    {% for i in range(stage) %}
        {% for j in range(stage) if j < i and a[i,j] != 0 %}
    a{{i+1}}{{j+1}} = {{a[i,j]}}
        {% endfor %}
    {% endfor -%}

    {% for i in range(stage) if b[i] != 0 %}
    b{{i+1}} = {{b[i]}}
    {% endfor -%}
    
    {% for i in range(stage) if c[i] != 0 %}
    c{{i+1}} = {{c[i]}}
    {% endfor %}

    if eqn == 1:
    {% for i in range(stage) %}
        k{{i+1}} = 0.0
    {% endfor %}
    else:
    {% for i in range(stage) %}
        k{{i+1}} = np.zeros(eqn)
    {% endfor %}
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        {% for i in range(stage) %}
        k{{i+1}} = func(t{% if c[i] != 0 %} + h*c{{i+1}}{% endif %}, x{% if i > 0 %} + h*({% endif %}
            {%- for j in range(stage) if j < i and a[i,j] != 0 -%}
                k{{j+1}}*a{{i+1}}{{j+1}}{% if not loop.last %} + {% endif %}
            {% endfor -%}{% if i > 0 %}){% endif %}, *params)
        {% endfor %}

        x = x + h*({% for j in range(stage) if b[j] != 0 %}{% if not loop.first %} + {% endif %}b{{j+1}}*k{{j+1}}{% endfor %})

    return times, np.array(xs).T