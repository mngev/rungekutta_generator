from jinja2 import Environment, FileSystemLoader, select_autoescape
import os.path

from ButcherTable import RRKTable
from settings import JULIA, MIN_ORDER

tables = RRKTable.from_json(JULIA['RRK']['tables'])


env = Environment(loader=FileSystemLoader(JULIA['RRK']['templates']['path']),
                  trim_blocks=True, lstrip_blocks=True)

method = env.get_template(JULIA['RRK']['templates']['method'])
doc_string = env.get_template(JULIA['RRK']['templates']['doc'])
julia_module = env.get_template(JULIA['RRK']['templates']['module'])

methods_list = []
source_code = []
for table in (tb for tb in tables if tb.p >= MIN_ORDER):
    methods_list.append(table.name)
    doc = doc_string.render(function_name=table.name)

    rrk = method.render(
        function_name=table.name,
        stage=table.s,
        order=table.p,
        eorder=table.ep,
        a=table.a,
        b=table.b,
        c=table.c,
        a_hat=table.a_hat,
        b_hat=table.b_hat,
        c_hat=table.c_hat)

    doc = '"""\n{0:s}\n"""'.format(doc)
    source_code.append('{0}\n{1}\n\n'.format(doc, rrk))

julia_module = julia_module.render(methods_list=methods_list)

with open(JULIA['RRK']['out'], 'w', encoding='utf-8') as file1,\
     open(JULIA['RRK']['out'][:-3]+'_base.jl', 'w', encoding='utf-8') as file2:
    
    file1.write(julia_module)
    file2.write("".join(source_code))
