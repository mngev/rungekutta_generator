from jinja2 import Environment, FileSystemLoader, select_autoescape
import os.path

from ButcherTable import ButcherTable
from settings import JULIA, MIN_ORDER


tables = ButcherTable.from_json(JULIA['RK']['tables'])
env = Environment(loader=FileSystemLoader(JULIA['RK']['templates']['path']),
                  trim_blocks=True, lstrip_blocks=True)

method = env.get_template(JULIA['RK']['templates']['method'])
doc_string = env.get_template(JULIA['RK']['templates']['doc'])
julia_module = env.get_template(JULIA['RK']['templates']['module'])

methods_list = []
source_code = []
for table in (tb for tb in tables if tb.p >= MIN_ORDER):
    methods_list.append(table.name)
    doc = doc_string.render(function_name=table.name)

    rk1 = method.render(function_name=table.name, stage=table.s,
                       a=table.a, b=table.b, c=table.c)
    rk2 = method.render(function_name=table.name, stage=table.s,
                       a=table.a, b=table.b, c=table.c, only_last=True)

    doc = '"""\n{0:s}\n"""'.format(doc)
    source_code.append('{0}\n{1}\n\n{2}\n'.format(doc, rk1, rk2))

julia_module = julia_module.render(methods_list=methods_list,
                                   source_code="".join(source_code))

with open(JULIA['RK']['out'], 'w', encoding='utf-8') as file:
    file.write(julia_module)
