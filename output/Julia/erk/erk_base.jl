"""
Embedded Runge--Kutta with base method's order 2, 
extrapolation order 3 and numbers of stages 3.
With step control and without dense output.

```
ERK23(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
ERK23(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function ERK23(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 1
  local a31 = 0.25
  local a32 = 0.25

  local b1 = 0.5
  local b2 = 0.5

  local b_hat1 = 0.16666666666666666
  local b_hat2 = 0.16666666666666666
  local b_hat3 = 0.66666666666666663

  local c2 = 1
  local c3 = 0.5

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 2, 3)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)

    x = x_0 + h*(b1*k1 + b2*k2)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat2*k2 + b_hat3*k3)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function ERK23

function ERK23(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 1
  local a31 = 0.25
  local a32 = 0.25

  local b1 = 0.5
  local b2 = 0.5

  local b_hat1 = 0.16666666666666666
  local b_hat2 = 0.16666666666666666
  local b_hat3 = 0.66666666666666663

  local c2 = 1
  local c3 = 0.5

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 2, 3)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)

    x = x_0 + h*(b1*k1 + b2*k2)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat2*k2 + b_hat3*k3)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function ERK23

"""
Embedded Runge--Kutta with base method's order 2, 
extrapolation order 3 and numbers of stages 4.
With step control and without dense output.

```
ERK23B(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
ERK23B(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function ERK23B(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.25
  local a31 = -0.23624999999999999
  local a32 = 0.91125
  local a41 = 0.24017957351290684
  local a42 = 0.030303030303030304
  local a43 = 0.72951739618406286

  local b1 = 0.24017957351290684
  local b2 = 0.030303030303030304
  local b3 = 0.72951739618406286

  local b_hat1 = 0.25308641975308643
  local b_hat3 = 0.75973409306742645
  local b_hat4 = -0.01282051282051282

  local c2 = 0.25
  local c3 = 0.67500000000000004
  local c4 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 2, 3)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)

    x = x_0 + h*(b1*k1 + b2*k2 + b3*k3)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function ERK23B

function ERK23B(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.25
  local a31 = -0.23624999999999999
  local a32 = 0.91125
  local a41 = 0.24017957351290684
  local a42 = 0.030303030303030304
  local a43 = 0.72951739618406286

  local b1 = 0.24017957351290684
  local b2 = 0.030303030303030304
  local b3 = 0.72951739618406286

  local b_hat1 = 0.25308641975308643
  local b_hat3 = 0.75973409306742645
  local b_hat4 = -0.01282051282051282

  local c2 = 0.25
  local c3 = 0.67500000000000004
  local c4 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 2, 3)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)

    x = x_0 + h*(b1*k1 + b2*k2 + b3*k3)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function ERK23B

"""
Embedded Runge--Kutta with base method's order 3, 
extrapolation order 2 and numbers of stages 4.
With step control and without dense output.

```
ERK32(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
ERK32(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function ERK32(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.5
  local a32 = 0.75
  local a41 = 0.22222222222222221
  local a42 = 0.33333333333333331
  local a43 = 0.44444444444444442

  local b1 = 0.22222222222222221
  local b2 = 0.33333333333333331
  local b3 = 0.44444444444444442

  local b_hat1 = 0.29166666666666669
  local b_hat2 = 0.25
  local b_hat3 = 0.33333333333333331
  local b_hat4 = 0.125

  local c2 = 0.5
  local c3 = 0.75
  local c4 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 3, 2)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)

    x = x_0 + h*(b1*k1 + b2*k2 + b3*k3)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat2*k2 + b_hat3*k3 + b_hat4*k4)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function ERK32

function ERK32(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.5
  local a32 = 0.75
  local a41 = 0.22222222222222221
  local a42 = 0.33333333333333331
  local a43 = 0.44444444444444442

  local b1 = 0.22222222222222221
  local b2 = 0.33333333333333331
  local b3 = 0.44444444444444442

  local b_hat1 = 0.29166666666666669
  local b_hat2 = 0.25
  local b_hat3 = 0.33333333333333331
  local b_hat4 = 0.125

  local c2 = 0.5
  local c3 = 0.75
  local c4 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 3, 2)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)

    x = x_0 + h*(b1*k1 + b2*k2 + b3*k3)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat2*k2 + b_hat3*k3 + b_hat4*k4)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function ERK32

"""
Embedded Runge--Kutta with base method's order 4, 
extrapolation order 3 and numbers of stages 5.
With step control and without dense output.

```
ERK43(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
ERK43(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function ERK43(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.5
  local a32 = 0.5
  local a43 = 1
  local a51 = 0.15625
  local a52 = 0.21875
  local a53 = 0.40625
  local a54 = -0.03125

  local b1 = 0.16666666666666666
  local b2 = 0.33333333333333331
  local b3 = 0.33333333333333331
  local b4 = 0.16666666666666666

  local b_hat1 = -0.5
  local b_hat2 = 2.3333333333333335
  local b_hat3 = 2.3333333333333335
  local b_hat4 = 2.1666666666666665
  local b_hat5 = -5.333333333333333

  local c2 = 0.5
  local c3 = 0.5
  local c4 = 1
  local c5 = 0.75

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 3)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)

    x = x_0 + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat2*k2 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function ERK43

function ERK43(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.5
  local a32 = 0.5
  local a43 = 1
  local a51 = 0.15625
  local a52 = 0.21875
  local a53 = 0.40625
  local a54 = -0.03125

  local b1 = 0.16666666666666666
  local b2 = 0.33333333333333331
  local b3 = 0.33333333333333331
  local b4 = 0.16666666666666666

  local b_hat1 = -0.5
  local b_hat2 = 2.3333333333333335
  local b_hat3 = 2.3333333333333335
  local b_hat4 = 2.1666666666666665
  local b_hat5 = -5.333333333333333

  local c2 = 0.5
  local c3 = 0.5
  local c4 = 1
  local c5 = 0.75

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 3)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)

    x = x_0 + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat2*k2 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function ERK43

"""
Embedded Runge--Kutta with base method's order 4, 
extrapolation order 3 and numbers of stages 5.
With step control and without dense output.

```
ERK43B(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
ERK43B(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function ERK43B(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.33333333333333331
  local a31 = -0.33333333333333331
  local a32 = 1
  local a41 = 1
  local a42 = -1
  local a43 = 1
  local a51 = 0.125
  local a52 = 0.375
  local a53 = 0.375
  local a54 = 0.125

  local b1 = 0.125
  local b2 = 0.375
  local b3 = 0.375
  local b4 = 0.125

  local b_hat1 = 0.083333333333333329
  local b_hat2 = 0.5
  local b_hat3 = 0.25
  local b_hat5 = 0.16666666666666666

  local c2 = 0.33333333333333331
  local c3 = 0.66666666666666663
  local c4 = 1
  local c5 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 3)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)

    x = x_0 + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat2*k2 + b_hat3*k3 + b_hat5*k5)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function ERK43B

function ERK43B(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.33333333333333331
  local a31 = -0.33333333333333331
  local a32 = 1
  local a41 = 1
  local a42 = -1
  local a43 = 1
  local a51 = 0.125
  local a52 = 0.375
  local a53 = 0.375
  local a54 = 0.125

  local b1 = 0.125
  local b2 = 0.375
  local b3 = 0.375
  local b4 = 0.125

  local b_hat1 = 0.083333333333333329
  local b_hat2 = 0.5
  local b_hat3 = 0.25
  local b_hat5 = 0.16666666666666666

  local c2 = 0.33333333333333331
  local c3 = 0.66666666666666663
  local c4 = 1
  local c5 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 3)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)

    x = x_0 + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat2*k2 + b_hat3*k3 + b_hat5*k5)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function ERK43B

"""
Embedded Runge--Kutta with base method's order 4, 
extrapolation order 5 and numbers of stages 5.
With step control and without dense output.

```
ERK45(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
ERK45(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function ERK45(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.33333333333333331
  local a31 = 0.16666666666666666
  local a32 = 0.16666666666666666
  local a41 = 0.125
  local a43 = 0.375
  local a51 = 0.5
  local a53 = -0.375
  local a54 = 2

  local b1 = 0.5
  local b3 = -1.5
  local b4 = 2

  local b_hat1 = 0.16666666666666666
  local b_hat4 = 0.66666666666666663
  local b_hat5 = 0.16666666666666666

  local c2 = 0.33333333333333331
  local c3 = 0.33333333333333331
  local c4 = 0.5
  local c5 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 5)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat4*k4 + b_hat5*k5)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function ERK45

function ERK45(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.33333333333333331
  local a31 = 0.16666666666666666
  local a32 = 0.16666666666666666
  local a41 = 0.125
  local a43 = 0.375
  local a51 = 0.5
  local a53 = -0.375
  local a54 = 2

  local b1 = 0.5
  local b3 = -1.5
  local b4 = 2

  local b_hat1 = 0.16666666666666666
  local b_hat4 = 0.66666666666666663
  local b_hat5 = 0.16666666666666666

  local c2 = 0.33333333333333331
  local c3 = 0.33333333333333331
  local c4 = 0.5
  local c5 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 5)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat4*k4 + b_hat5*k5)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function ERK45

"""
Embedded Runge--Kutta with base method's order 5, 
extrapolation order 4 and numbers of stages 6.
With step control and without dense output.

```
DPRK546S(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
DPRK546S(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function DPRK546S(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.20000000000000001
  local a31 = 0.074999999999999997
  local a32 = 0.22500000000000001
  local a41 = 0.29999999999999999
  local a42 = -0.90000000000000002
  local a43 = 1.2
  local a51 = 0.31001371742112482
  local a52 = -0.92592592592592593
  local a53 = 1.2071330589849107
  local a54 = 0.075445816186556922
  local a61 = -0.67037037037037039
  local a62 = 2.5
  local a63 = -0.89562289562289565
  local a64 = -3.3703703703703702
  local a65 = 3.4363636363636365

  local b1 = 0.057407407407407407
  local b3 = 0.63973063973063971
  local b4 = -1.3425925925925926
  local b5 = 1.5954545454545455
  local b6 = 0.050000000000000003

  local b_hat1 = 0.087962962962962965
  local b_hat3 = 0.48100048100048098
  local b_hat4 = -0.57870370370370372
  local b_hat5 = 0.92045454545454541
  local b_hat6 = 0.089285714285714288

  local c2 = 0.20000000000000001
  local c3 = 0.29999999999999999
  local c4 = 0.59999999999999998
  local c5 = 0.66666666666666663
  local c6 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 5, 4)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function DPRK546S

function DPRK546S(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.20000000000000001
  local a31 = 0.074999999999999997
  local a32 = 0.22500000000000001
  local a41 = 0.29999999999999999
  local a42 = -0.90000000000000002
  local a43 = 1.2
  local a51 = 0.31001371742112482
  local a52 = -0.92592592592592593
  local a53 = 1.2071330589849107
  local a54 = 0.075445816186556922
  local a61 = -0.67037037037037039
  local a62 = 2.5
  local a63 = -0.89562289562289565
  local a64 = -3.3703703703703702
  local a65 = 3.4363636363636365

  local b1 = 0.057407407407407407
  local b3 = 0.63973063973063971
  local b4 = -1.3425925925925926
  local b5 = 1.5954545454545455
  local b6 = 0.050000000000000003

  local b_hat1 = 0.087962962962962965
  local b_hat3 = 0.48100048100048098
  local b_hat4 = -0.57870370370370372
  local b_hat5 = 0.92045454545454541
  local b_hat6 = 0.089285714285714288

  local c2 = 0.20000000000000001
  local c3 = 0.29999999999999999
  local c4 = 0.59999999999999998
  local c5 = 0.66666666666666663
  local c6 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 5, 4)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function DPRK546S

"""
Embedded Runge--Kutta with base method's order 5, 
extrapolation order 4 and numbers of stages 7.
With step control and without dense output.

```
DPRK547S(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
DPRK547S(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function DPRK547S(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.22222222222222221
  local a31 = 0.083333333333333329
  local a32 = 0.25
  local a41 = 0.16975308641975309
  local a42 = -0.23148148148148148
  local a43 = 0.61728395061728392
  local a51 = 0.25151515151515152
  local a52 = -0.59090909090909094
  local a53 = 0.9242424242424242
  local a54 = 0.081818181818181818
  local a61 = -0.6785714285714286
  local a62 = 2.25
  local a63 = 0.14285714285714285
  local a64 = -3.8571428571428572
  local a65 = 3.1428571428571428
  local a71 = 0.095000000000000001
  local a73 = 0.59999999999999998
  local a74 = -0.60750000000000004
  local a75 = 0.82499999999999996
  local a76 = 0.087499999999999994

  local b1 = 0.086199999999999999
  local b3 = 0.66600000000000004
  local b4 = -0.78569999999999995
  local b5 = 0.95699999999999996
  local b6 = 0.096500000000000002
  local b7 = -0.02

  local b_hat1 = 0.095000000000000001
  local b_hat3 = 0.59999999999999998
  local b_hat4 = -0.60750000000000004
  local b_hat5 = 0.82499999999999996
  local b_hat6 = 0.087499999999999994

  local c2 = 0.22222222222222221
  local c3 = 0.33333333333333331
  local c4 = 0.55555555555555558
  local c5 = 0.66666666666666663
  local c6 = 1
  local c7 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 5, 4)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k3*a73 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6 + b7*k7)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function DPRK547S

function DPRK547S(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.22222222222222221
  local a31 = 0.083333333333333329
  local a32 = 0.25
  local a41 = 0.16975308641975309
  local a42 = -0.23148148148148148
  local a43 = 0.61728395061728392
  local a51 = 0.25151515151515152
  local a52 = -0.59090909090909094
  local a53 = 0.9242424242424242
  local a54 = 0.081818181818181818
  local a61 = -0.6785714285714286
  local a62 = 2.25
  local a63 = 0.14285714285714285
  local a64 = -3.8571428571428572
  local a65 = 3.1428571428571428
  local a71 = 0.095000000000000001
  local a73 = 0.59999999999999998
  local a74 = -0.60750000000000004
  local a75 = 0.82499999999999996
  local a76 = 0.087499999999999994

  local b1 = 0.086199999999999999
  local b3 = 0.66600000000000004
  local b4 = -0.78569999999999995
  local b5 = 0.95699999999999996
  local b6 = 0.096500000000000002
  local b7 = -0.02

  local b_hat1 = 0.095000000000000001
  local b_hat3 = 0.59999999999999998
  local b_hat4 = -0.60750000000000004
  local b_hat5 = 0.82499999999999996
  local b_hat6 = 0.087499999999999994

  local c2 = 0.22222222222222221
  local c3 = 0.33333333333333331
  local c4 = 0.55555555555555558
  local c5 = 0.66666666666666663
  local c6 = 1
  local c7 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 5, 4)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k3*a73 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6 + b7*k7)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function DPRK547S

"""
Embedded Runge--Kutta with base method's order 6, 
extrapolation order 5 and numbers of stages 8.
With step control and without dense output.

```
DPRK658M(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
DPRK658M(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function DPRK658M(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.10000000000000001
  local a31 = -0.024691358024691357
  local a32 = 0.24691358024691357
  local a41 = 0.44825072886297374
  local a42 = -0.78717201166180761
  local a43 = 0.76749271137026243
  local a51 = 0.58963636363636363
  local a52 = -0.98181818181818181
  local a53 = 0.71257342657342659
  local a54 = 0.27960839160839163
  local a61 = -0.71358922558922555
  local a62 = 1.3090909090909091
  local a63 = 0.12012834224598931
  local a64 = -0.65201346801346804
  local a65 = 0.73638344226579522
  local a71 = 2.3404882154882154
  local a72 = -3.1818181818181817
  local a73 = -0.76312375407398036
  local a74 = 4.482612117227502
  local a75 = -2.8458605664488017
  local a76 = 0.96770216962524658
  local a81 = 1.7491394600769601
  local a82 = -2.3904220779220777
  local a83 = -0.39625257378368239
  local a84 = 3.2728583293487139
  local a85 = -2.0635163787737318
  local a86 = 0.82819324105381797

  local b1 = 0.076018518518518513
  local b3 = 0.27404107205012185
  local b4 = 0.19205895244356783
  local b5 = 0.10757080610021787
  local b6 = 0.29031065088757396
  local b7 = 0.059999999999999998

  local b_hat1 = 0.070601851851851846
  local b_hat3 = 0.30584941077022526
  local b_hat4 = 0.11510382423843962
  local b_hat5 = 0.18722766884531591
  local b_hat6 = 0.25425295857988167
  local b_hat7 = -0.033035714285714286
  local b_hat8 = 0.10000000000000001

  local c2 = 0.10000000000000001
  local c3 = 0.22222222222222221
  local c4 = 0.42857142857142855
  local c5 = 0.59999999999999998
  local c6 = 0.80000000000000004
  local c7 = 1
  local c8 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 6, 5)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k2*a72 + k3*a73 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k2*a82 + k3*a83 + k4*a84 + k5*a85 + k6*a86)
    k8 = func(t + h*c8, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6 + b7*k7)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6 + b_hat7*k7 + b_hat8*k8)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function DPRK658M

function DPRK658M(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.10000000000000001
  local a31 = -0.024691358024691357
  local a32 = 0.24691358024691357
  local a41 = 0.44825072886297374
  local a42 = -0.78717201166180761
  local a43 = 0.76749271137026243
  local a51 = 0.58963636363636363
  local a52 = -0.98181818181818181
  local a53 = 0.71257342657342659
  local a54 = 0.27960839160839163
  local a61 = -0.71358922558922555
  local a62 = 1.3090909090909091
  local a63 = 0.12012834224598931
  local a64 = -0.65201346801346804
  local a65 = 0.73638344226579522
  local a71 = 2.3404882154882154
  local a72 = -3.1818181818181817
  local a73 = -0.76312375407398036
  local a74 = 4.482612117227502
  local a75 = -2.8458605664488017
  local a76 = 0.96770216962524658
  local a81 = 1.7491394600769601
  local a82 = -2.3904220779220777
  local a83 = -0.39625257378368239
  local a84 = 3.2728583293487139
  local a85 = -2.0635163787737318
  local a86 = 0.82819324105381797

  local b1 = 0.076018518518518513
  local b3 = 0.27404107205012185
  local b4 = 0.19205895244356783
  local b5 = 0.10757080610021787
  local b6 = 0.29031065088757396
  local b7 = 0.059999999999999998

  local b_hat1 = 0.070601851851851846
  local b_hat3 = 0.30584941077022526
  local b_hat4 = 0.11510382423843962
  local b_hat5 = 0.18722766884531591
  local b_hat6 = 0.25425295857988167
  local b_hat7 = -0.033035714285714286
  local b_hat8 = 0.10000000000000001

  local c2 = 0.10000000000000001
  local c3 = 0.22222222222222221
  local c4 = 0.42857142857142855
  local c5 = 0.59999999999999998
  local c6 = 0.80000000000000004
  local c7 = 1
  local c8 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 6, 5)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k2*a72 + k3*a73 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k2*a82 + k3*a83 + k4*a84 + k5*a85 + k6*a86)
    k8 = func(t + h*c8, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6 + b7*k7)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6 + b_hat7*k7 + b_hat8*k8)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function DPRK658M

"""
Embedded Runge--Kutta with base method's order 4, 
extrapolation order 5 and numbers of stages 6.
With step control and without dense output.

```
Fehlberg45(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
Fehlberg45(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function Fehlberg45(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.25
  local a31 = 0.09375
  local a32 = 0.28125
  local a41 = 0.87938097405553028
  local a42 = -3.2771961766044608
  local a43 = 3.3208921256258535
  local a51 = 2.0324074074074074
  local a52 = -8
  local a53 = 7.1734892787524362
  local a54 = -0.20589668615984405
  local a61 = -0.29629629629629628
  local a62 = 2
  local a63 = -1.3816764132553607
  local a64 = 0.45297270955165692
  local a65 = -0.27500000000000002

  local b1 = 0.11574074074074074
  local b3 = 0.54892787524366471
  local b4 = 0.53533138401559455
  local b5 = -0.20000000000000001

  local b_hat1 = 0.11851851851851852
  local b_hat3 = 0.51898635477582844
  local b_hat4 = 0.50613149034201665
  local b_hat5 = -0.17999999999999999
  local b_hat6 = 0.036363636363636362

  local c2 = 0.25
  local c3 = 0.375
  local c4 = 0.92307692307692313
  local c5 = 1
  local c6 = 0.5

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 5)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function Fehlberg45

function Fehlberg45(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.25
  local a31 = 0.09375
  local a32 = 0.28125
  local a41 = 0.87938097405553028
  local a42 = -3.2771961766044608
  local a43 = 3.3208921256258535
  local a51 = 2.0324074074074074
  local a52 = -8
  local a53 = 7.1734892787524362
  local a54 = -0.20589668615984405
  local a61 = -0.29629629629629628
  local a62 = 2
  local a63 = -1.3816764132553607
  local a64 = 0.45297270955165692
  local a65 = -0.27500000000000002

  local b1 = 0.11574074074074074
  local b3 = 0.54892787524366471
  local b4 = 0.53533138401559455
  local b5 = -0.20000000000000001

  local b_hat1 = 0.11851851851851852
  local b_hat3 = 0.51898635477582844
  local b_hat4 = 0.50613149034201665
  local b_hat5 = -0.17999999999999999
  local b_hat6 = 0.036363636363636362

  local c2 = 0.25
  local c3 = 0.375
  local c4 = 0.92307692307692313
  local c5 = 1
  local c6 = 0.5

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 5)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function Fehlberg45

"""
Embedded Runge--Kutta with base method's order 4, 
extrapolation order 5 and numbers of stages 6.
With step control and without dense output.

```
CashKarp45(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
CashKarp45(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function CashKarp45(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.20000000000000001
  local a31 = 0.074999999999999997
  local a32 = 0.22500000000000001
  local a41 = 0.29999999999999999
  local a42 = -0.90000000000000002
  local a43 = 1.2
  local a51 = -0.20370370370370369
  local a52 = 2.5
  local a53 = -2.5925925925925926
  local a54 = 1.2962962962962963
  local a61 = 0.029495804398148147
  local a62 = 0.341796875
  local a63 = 0.041594328703703706
  local a64 = 0.40033455400334556
  local a65 = 0.061767578125

  local b1 = 0.097883597883597878
  local b3 = 0.40257648953301128
  local b4 = 0.21043771043771045
  local b6 = 0.28910220214568039

  local b_hat1 = 0.10217737268518519
  local b_hat3 = 0.38390790343915343
  local b_hat4 = 0.24459273726851852
  local b_hat5 = 0.019321986607142856
  local b_hat6 = 0.25

  local c2 = 0.20000000000000001
  local c3 = 0.29999999999999999
  local c4 = 0.59999999999999998
  local c5 = 1
  local c6 = 0.875

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 5)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b6*k6)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function CashKarp45

function CashKarp45(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.20000000000000001
  local a31 = 0.074999999999999997
  local a32 = 0.22500000000000001
  local a41 = 0.29999999999999999
  local a42 = -0.90000000000000002
  local a43 = 1.2
  local a51 = -0.20370370370370369
  local a52 = 2.5
  local a53 = -2.5925925925925926
  local a54 = 1.2962962962962963
  local a61 = 0.029495804398148147
  local a62 = 0.341796875
  local a63 = 0.041594328703703706
  local a64 = 0.40033455400334556
  local a65 = 0.061767578125

  local b1 = 0.097883597883597878
  local b3 = 0.40257648953301128
  local b4 = 0.21043771043771045
  local b6 = 0.28910220214568039

  local b_hat1 = 0.10217737268518519
  local b_hat3 = 0.38390790343915343
  local b_hat4 = 0.24459273726851852
  local b_hat5 = 0.019321986607142856
  local b_hat6 = 0.25

  local c2 = 0.20000000000000001
  local c3 = 0.29999999999999999
  local c4 = 0.59999999999999998
  local c5 = 1
  local c6 = 0.875

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 5)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b6*k6)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function CashKarp45

"""
Embedded Runge--Kutta with base method's order 5, 
extrapolation order 4 and numbers of stages 7.
With step control and without dense output.

```
DOPRI5(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
DOPRI5(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function DOPRI5(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.20000000000000001
  local a31 = 0.074999999999999997
  local a32 = 0.22500000000000001
  local a41 = 0.97777777777777775
  local a42 = -3.7333333333333334
  local a43 = 3.5555555555555554
  local a51 = 2.9525986892242035
  local a52 = -11.595793324188385
  local a53 = 9.8228928516994358
  local a54 = -0.29080932784636487
  local a61 = 2.8462752525252526
  local a62 = -10.757575757575758
  local a63 = 8.9064227177434727
  local a64 = 0.27840909090909088
  local a65 = -0.2735313036020583
  local a71 = 0.091145833333333329
  local a73 = 0.44923629829290207
  local a74 = 0.65104166666666663
  local a75 = -0.322376179245283
  local a76 = 0.13095238095238096

  local b1 = 0.091145833333333329
  local b3 = 0.44923629829290207
  local b4 = 0.65104166666666663
  local b5 = -0.322376179245283
  local b6 = 0.13095238095238096

  local b_hat1 = 0.089913194444444441
  local b_hat3 = 0.45348906858340821
  local b_hat4 = 0.61406249999999996
  local b_hat5 = -0.27151238207547168
  local b_hat6 = 0.089047619047619042
  local b_hat7 = 0.025000000000000001

  local c2 = 0.20000000000000001
  local c3 = 0.29999999999999999
  local c4 = 0.80000000000000004
  local c5 = 0.88888888888888884
  local c6 = 1
  local c7 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 5, 4)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k3*a73 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6 + b_hat7*k7)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function DOPRI5

function DOPRI5(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.20000000000000001
  local a31 = 0.074999999999999997
  local a32 = 0.22500000000000001
  local a41 = 0.97777777777777775
  local a42 = -3.7333333333333334
  local a43 = 3.5555555555555554
  local a51 = 2.9525986892242035
  local a52 = -11.595793324188385
  local a53 = 9.8228928516994358
  local a54 = -0.29080932784636487
  local a61 = 2.8462752525252526
  local a62 = -10.757575757575758
  local a63 = 8.9064227177434727
  local a64 = 0.27840909090909088
  local a65 = -0.2735313036020583
  local a71 = 0.091145833333333329
  local a73 = 0.44923629829290207
  local a74 = 0.65104166666666663
  local a75 = -0.322376179245283
  local a76 = 0.13095238095238096

  local b1 = 0.091145833333333329
  local b3 = 0.44923629829290207
  local b4 = 0.65104166666666663
  local b5 = -0.322376179245283
  local b6 = 0.13095238095238096

  local b_hat1 = 0.089913194444444441
  local b_hat3 = 0.45348906858340821
  local b_hat4 = 0.61406249999999996
  local b_hat5 = -0.27151238207547168
  local b_hat6 = 0.089047619047619042
  local b_hat7 = 0.025000000000000001

  local c2 = 0.20000000000000001
  local c3 = 0.29999999999999999
  local c4 = 0.80000000000000004
  local c5 = 0.88888888888888884
  local c6 = 1
  local c7 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 5, 4)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k3*a73 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6 + b_hat7*k7)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function DOPRI5

"""
Embedded Runge--Kutta with base method's order 6, 
extrapolation order 5 and numbers of stages 8.
With step control and without dense output.

```
DVERK65(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
DVERK65(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function DVERK65(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.16666666666666666
  local a31 = 0.053333333333333337
  local a32 = 0.21333333333333335
  local a41 = 0.83333333333333337
  local a42 = -2.6666666666666665
  local a43 = 2.5
  local a51 = -2.578125
  local a52 = 9.1666666666666661
  local a53 = -6.640625
  local a54 = 0.88541666666666663
  local a61 = 2.3999999999999999
  local a62 = -8
  local a63 = 6.5604575163398691
  local a64 = -0.30555555555555558
  local a65 = 0.34509803921568627
  local a71 = -0.55086666666666662
  local a72 = 1.6533333333333333
  local a73 = -0.94558823529411762
  local a74 = -0.32400000000000001
  local a75 = 0.23378823529411766
  local a81 = 2.0354651162790698
  local a82 = -6.9767441860465116
  local a83 = 5.6481798145614839
  local a84 = -0.13738156761412576
  local a85 = 0.28630226610361031
  local a87 = 0.14417855671647381

  local b1 = 0.074999999999999997
  local b3 = 0.38992869875222819
  local b4 = 0.31944444444444442
  local b5 = 0.13503836317135551
  local b7 = 0.010783298826777088
  local b8 = 0.069805194805194801

  local b_hat1 = 0.081250000000000003
  local b_hat3 = 0.39689171122994654
  local b_hat4 = 0.3125
  local b_hat5 = 0.14117647058823529
  local b_hat6 = 0.068181818181818177

  local c2 = 0.16666666666666666
  local c3 = 0.26666666666666666
  local c4 = 0.66666666666666663
  local c5 = 0.83333333333333337
  local c6 = 1
  local c7 = 0.066666666666666666
  local c8 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 6, 5)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k2*a72 + k3*a73 + k4*a74 + k5*a75)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k2*a82 + k3*a83 + k4*a84 + k5*a85 + k7*a87)
    k8 = func(t + h*c8, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b7*k7 + b8*k8)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function DVERK65

function DVERK65(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.16666666666666666
  local a31 = 0.053333333333333337
  local a32 = 0.21333333333333335
  local a41 = 0.83333333333333337
  local a42 = -2.6666666666666665
  local a43 = 2.5
  local a51 = -2.578125
  local a52 = 9.1666666666666661
  local a53 = -6.640625
  local a54 = 0.88541666666666663
  local a61 = 2.3999999999999999
  local a62 = -8
  local a63 = 6.5604575163398691
  local a64 = -0.30555555555555558
  local a65 = 0.34509803921568627
  local a71 = -0.55086666666666662
  local a72 = 1.6533333333333333
  local a73 = -0.94558823529411762
  local a74 = -0.32400000000000001
  local a75 = 0.23378823529411766
  local a81 = 2.0354651162790698
  local a82 = -6.9767441860465116
  local a83 = 5.6481798145614839
  local a84 = -0.13738156761412576
  local a85 = 0.28630226610361031
  local a87 = 0.14417855671647381

  local b1 = 0.074999999999999997
  local b3 = 0.38992869875222819
  local b4 = 0.31944444444444442
  local b5 = 0.13503836317135551
  local b7 = 0.010783298826777088
  local b8 = 0.069805194805194801

  local b_hat1 = 0.081250000000000003
  local b_hat3 = 0.39689171122994654
  local b_hat4 = 0.3125
  local b_hat5 = 0.14117647058823529
  local b_hat6 = 0.068181818181818177

  local c2 = 0.16666666666666666
  local c3 = 0.26666666666666666
  local c4 = 0.66666666666666663
  local c5 = 0.83333333333333337
  local c6 = 1
  local c7 = 0.066666666666666666
  local c8 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 6, 5)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k2*a42 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k2*a72 + k3*a73 + k4*a74 + k5*a75)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k2*a82 + k3*a83 + k4*a84 + k5*a85 + k7*a87)
    k8 = func(t + h*c8, x)

    x = x_0 + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b7*k7 + b8*k8)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat3*k3 + b_hat4*k4 + b_hat5*k5 + b_hat6*k6)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function DVERK65

"""
Embedded Runge--Kutta with base method's order 7, 
extrapolation order 8 and numbers of stages 13.
With step control and without dense output.

```
Fehlberg78A(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
Fehlberg78A(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function Fehlberg78A(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.07407407407407407
  local a31 = 0.027777777777777776
  local a32 = 0.083333333333333329
  local a41 = 0.041666666666666664
  local a43 = 0.125
  local a51 = 0.41666666666666669
  local a53 = -1.5625
  local a54 = 1.5625
  local a61 = 0.050000000000000003
  local a64 = 0.25
  local a65 = 0.20000000000000001
  local a71 = -0.23148148148148148
  local a74 = 1.1574074074074074
  local a75 = -2.4074074074074074
  local a76 = 2.3148148148148149
  local a81 = 0.10333333333333333
  local a85 = 0.27111111111111114
  local a86 = -0.22222222222222221
  local a87 = 0.014444444444444444
  local a91 = 2
  local a94 = -8.8333333333333339
  local a95 = 15.644444444444444
  local a96 = -11.888888888888889
  local a97 = 0.74444444444444446
  local a98 = 3
  local a101 = -0.84259259259259256
  local a104 = 0.21296296296296297
  local a105 = -7.2296296296296294
  local a106 = 5.7592592592592595
  local a107 = -0.31666666666666665
  local a108 = 2.8333333333333335
  local a109 = -0.083333333333333329
  local a111 = 0.58121951219512191
  local a114 = -2.0792682926829267
  local a115 = 4.3863414634146345
  local a116 = -3.6707317073170733
  local a117 = 0.52024390243902441
  local a118 = 0.54878048780487809
  local a119 = 0.27439024390243905
  local a1110 = 0.43902439024390244
  local a121 = 0.014634146341463415
  local a126 = -0.14634146341463414
  local a127 = -0.014634146341463415
  local a128 = -0.073170731707317069
  local a129 = 0.073170731707317069
  local a1210 = 0.14634146341463414
  local a131 = -0.43341463414634146
  local a134 = -2.0792682926829267
  local a135 = 4.3863414634146345
  local a136 = -3.524390243902439
  local a137 = 0.53487804878048784
  local a138 = 0.62195121951219512
  local a139 = 0.20121951219512196
  local a1310 = 0.46341463414634149
  local a1312 = 1

  local b1 = 0.04880952380952381
  local b6 = 0.32380952380952382
  local b7 = 0.25714285714285712
  local b8 = 0.25714285714285712
  local b9 = 0.03214285714285714
  local b10 = 0.03214285714285714
  local b11 = 0.04880952380952381

  local b_hat6 = 0.32380952380952382
  local b_hat7 = 0.25714285714285712
  local b_hat8 = 0.25714285714285712
  local b_hat9 = 0.03214285714285714
  local b_hat10 = 0.03214285714285714
  local b_hat12 = 0.04880952380952381
  local b_hat13 = 0.04880952380952381

  local c2 = 0.07407407407407407
  local c3 = 0.1111111111111111
  local c4 = 0.16666666666666666
  local c5 = 0.41666666666666669
  local c6 = 0.5
  local c7 = 0.83333333333333337
  local c8 = 0.16666666666666666
  local c9 = 0.66666666666666663
  local c10 = 0.33333333333333331
  local c11 = 1
  local c13 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)
  local k9::Vector{Float64} = zeros(Float64, EQN)
  local k10::Vector{Float64} = zeros(Float64, EQN)
  local k11::Vector{Float64} = zeros(Float64, EQN)
  local k12::Vector{Float64} = zeros(Float64, EQN)
  local k13::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 7, 8)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k5*a85 + k6*a86 + k7*a87)
    k8 = func(t + h*c8, x)
    x = x_0 + h*(k1*a91 + k4*a94 + k5*a95 + k6*a96 + k7*a97 + k8*a98)
    k9 = func(t + h*c9, x)
    x = x_0 + h*(k1*a101 + k4*a104 + k5*a105 + k6*a106 + k7*a107 + k8*a108 + k9*a109)
    k10 = func(t + h*c10, x)
    x = x_0 + h*(k1*a111 + k4*a114 + k5*a115 + k6*a116 + k7*a117 + k8*a118 + k9*a119 + k10*a1110)
    k11 = func(t + h*c11, x)
    x = x_0 + h*(k1*a121 + k6*a126 + k7*a127 + k8*a128 + k9*a129 + k10*a1210)
    k12 = func(t, x)
    x = x_0 + h*(k1*a131 + k4*a134 + k5*a135 + k6*a136 + k7*a137 + k8*a138 + k9*a139 + k10*a1310 + k12*a1312)
    k13 = func(t + h*c13, x)

    x = x_0 + h*(b1*k1 + b6*k6 + b7*k7 + b8*k8 + b9*k9 + b10*k10 + b11*k11)
    x_hat = x_0 + h*(b_hat6*k6 + b_hat7*k7 + b_hat8*k8 + b_hat9*k9 + b_hat10*k10 + b_hat12*k12 + b_hat13*k13)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function Fehlberg78A

function Fehlberg78A(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.07407407407407407
  local a31 = 0.027777777777777776
  local a32 = 0.083333333333333329
  local a41 = 0.041666666666666664
  local a43 = 0.125
  local a51 = 0.41666666666666669
  local a53 = -1.5625
  local a54 = 1.5625
  local a61 = 0.050000000000000003
  local a64 = 0.25
  local a65 = 0.20000000000000001
  local a71 = -0.23148148148148148
  local a74 = 1.1574074074074074
  local a75 = -2.4074074074074074
  local a76 = 2.3148148148148149
  local a81 = 0.10333333333333333
  local a85 = 0.27111111111111114
  local a86 = -0.22222222222222221
  local a87 = 0.014444444444444444
  local a91 = 2
  local a94 = -8.8333333333333339
  local a95 = 15.644444444444444
  local a96 = -11.888888888888889
  local a97 = 0.74444444444444446
  local a98 = 3
  local a101 = -0.84259259259259256
  local a104 = 0.21296296296296297
  local a105 = -7.2296296296296294
  local a106 = 5.7592592592592595
  local a107 = -0.31666666666666665
  local a108 = 2.8333333333333335
  local a109 = -0.083333333333333329
  local a111 = 0.58121951219512191
  local a114 = -2.0792682926829267
  local a115 = 4.3863414634146345
  local a116 = -3.6707317073170733
  local a117 = 0.52024390243902441
  local a118 = 0.54878048780487809
  local a119 = 0.27439024390243905
  local a1110 = 0.43902439024390244
  local a121 = 0.014634146341463415
  local a126 = -0.14634146341463414
  local a127 = -0.014634146341463415
  local a128 = -0.073170731707317069
  local a129 = 0.073170731707317069
  local a1210 = 0.14634146341463414
  local a131 = -0.43341463414634146
  local a134 = -2.0792682926829267
  local a135 = 4.3863414634146345
  local a136 = -3.524390243902439
  local a137 = 0.53487804878048784
  local a138 = 0.62195121951219512
  local a139 = 0.20121951219512196
  local a1310 = 0.46341463414634149
  local a1312 = 1

  local b1 = 0.04880952380952381
  local b6 = 0.32380952380952382
  local b7 = 0.25714285714285712
  local b8 = 0.25714285714285712
  local b9 = 0.03214285714285714
  local b10 = 0.03214285714285714
  local b11 = 0.04880952380952381

  local b_hat6 = 0.32380952380952382
  local b_hat7 = 0.25714285714285712
  local b_hat8 = 0.25714285714285712
  local b_hat9 = 0.03214285714285714
  local b_hat10 = 0.03214285714285714
  local b_hat12 = 0.04880952380952381
  local b_hat13 = 0.04880952380952381

  local c2 = 0.07407407407407407
  local c3 = 0.1111111111111111
  local c4 = 0.16666666666666666
  local c5 = 0.41666666666666669
  local c6 = 0.5
  local c7 = 0.83333333333333337
  local c8 = 0.16666666666666666
  local c9 = 0.66666666666666663
  local c10 = 0.33333333333333331
  local c11 = 1
  local c13 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)
  local k9::Vector{Float64} = zeros(Float64, EQN)
  local k10::Vector{Float64} = zeros(Float64, EQN)
  local k11::Vector{Float64} = zeros(Float64, EQN)
  local k12::Vector{Float64} = zeros(Float64, EQN)
  local k13::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 7, 8)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k5*a85 + k6*a86 + k7*a87)
    k8 = func(t + h*c8, x)
    x = x_0 + h*(k1*a91 + k4*a94 + k5*a95 + k6*a96 + k7*a97 + k8*a98)
    k9 = func(t + h*c9, x)
    x = x_0 + h*(k1*a101 + k4*a104 + k5*a105 + k6*a106 + k7*a107 + k8*a108 + k9*a109)
    k10 = func(t + h*c10, x)
    x = x_0 + h*(k1*a111 + k4*a114 + k5*a115 + k6*a116 + k7*a117 + k8*a118 + k9*a119 + k10*a1110)
    k11 = func(t + h*c11, x)
    x = x_0 + h*(k1*a121 + k6*a126 + k7*a127 + k8*a128 + k9*a129 + k10*a1210)
    k12 = func(t, x)
    x = x_0 + h*(k1*a131 + k4*a134 + k5*a135 + k6*a136 + k7*a137 + k8*a138 + k9*a139 + k10*a1310 + k12*a1312)
    k13 = func(t + h*c13, x)

    x = x_0 + h*(b1*k1 + b6*k6 + b7*k7 + b8*k8 + b9*k9 + b10*k10 + b11*k11)
    x_hat = x_0 + h*(b_hat6*k6 + b_hat7*k7 + b_hat8*k8 + b_hat9*k9 + b_hat10*k10 + b_hat12*k12 + b_hat13*k13)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function Fehlberg78A

"""
Embedded Runge--Kutta with base method's order 7, 
extrapolation order 8 and numbers of stages 13.
With step control and without dense output.

```
Fehlberg78B(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
Fehlberg78B(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function Fehlberg78B(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.055555555555555552
  local a31 = 0.020833333333333332
  local a32 = 0.0625
  local a41 = 0.03125
  local a43 = 0.09375
  local a51 = 0.3125
  local a53 = -1.171875
  local a54 = 1.171875
  local a61 = 0.037499999999999999
  local a64 = 0.1875
  local a65 = 0.14999999999999999
  local a71 = 0.047910137111111112
  local a74 = 0.11224871277777777
  local a75 = -0.025505673777777779
  local a76 = 0.012846823888888888
  local a81 = 0.016917989787292281
  local a84 = 0.3878482784860432
  local a85 = 0.035977369851500331
  local a86 = 0.19697021421566607
  local a87 = -0.17271385234050185
  local a91 = 0.069095753359192297
  local a94 = -0.63424797672885413
  local a95 = -0.16119757522460407
  local a96 = 0.13865030945882525
  local a97 = 0.94092861403575623
  local a98 = 0.21163632648194397
  local a101 = 0.18355699683904539
  local a104 = -2.4687680843155926
  local a105 = -0.29128688781630047
  local a106 = -0.026473020233117376
  local a107 = 2.8478387641928005
  local a108 = 0.28138733146984979
  local a109 = 0.12374489986331466
  local a111 = -1.2154248173958881
  local a114 = 16.672608665945774
  local a115 = 0.91574182841681795
  local a116 = -6.0566058043574706
  local a117 = -16.00357359415618
  local a118 = 14.849303086297663
  local a119 = -13.371575735289849
  local a1110 = 5.134182648179638
  local a121 = 0.25886091643826425
  local a124 = -4.7744857854892047
  local a125 = -0.43509301377703252
  local a126 = -3.0494833320722416
  local a127 = 5.5779200399360995
  local a128 = 6.1558315898610401
  local a129 = -5.0621045867369387
  local a1210 = 2.193926173180679
  local a1211 = 0.13462799865933495
  local a131 = 0.82242759962650747
  local a134 = -11.658673257277664
  local a135 = -0.75762211669093615
  local a136 = 0.71397358815958156
  local a137 = 12.075774986890057
  local a138 = -2.1276591139204029
  local a139 = 1.9901662070489554
  local a1310 = -0.23428647154404028
  local a1311 = 0.17589857770794226

  local b1 = 0.029553213676353499
  local b6 = -0.82860627648779706
  local b7 = 0.31124090005111832
  local b8 = 2.4673451905998869
  local b9 = -2.5469416518419088
  local b10 = 1.4435485836767752
  local b11 = 0.079415595881127288
  local b12 = 0.044444444444444446

  local b_hat1 = 0.041747491141530244
  local b_hat6 = -0.055452328611239311
  local b_hat7 = 0.23931280720118009
  local b_hat8 = 0.70351066940344298
  local b_hat9 = -0.75975961381446089
  local b_hat10 = 0.6605630309222863
  local b_hat11 = 0.15818748251012332
  local b_hat12 = -0.23810953875286281
  local b_hat13 = 0.25

  local c2 = 0.055555555555555552
  local c3 = 0.083333333333333329
  local c4 = 0.125
  local c5 = 0.3125
  local c6 = 0.375
  local c7 = 0.14749999999999999
  local c8 = 0.46500000000000008
  local c9 = 0.56486545138225952
  local c10 = 0.64999999999999969
  local c11 = 0.92465627764050407
  local c12 = 1.0000000000000009
  local c13 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)
  local k9::Vector{Float64} = zeros(Float64, EQN)
  local k10::Vector{Float64} = zeros(Float64, EQN)
  local k11::Vector{Float64} = zeros(Float64, EQN)
  local k12::Vector{Float64} = zeros(Float64, EQN)
  local k13::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 7, 8)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k4*a84 + k5*a85 + k6*a86 + k7*a87)
    k8 = func(t + h*c8, x)
    x = x_0 + h*(k1*a91 + k4*a94 + k5*a95 + k6*a96 + k7*a97 + k8*a98)
    k9 = func(t + h*c9, x)
    x = x_0 + h*(k1*a101 + k4*a104 + k5*a105 + k6*a106 + k7*a107 + k8*a108 + k9*a109)
    k10 = func(t + h*c10, x)
    x = x_0 + h*(k1*a111 + k4*a114 + k5*a115 + k6*a116 + k7*a117 + k8*a118 + k9*a119 + k10*a1110)
    k11 = func(t + h*c11, x)
    x = x_0 + h*(k1*a121 + k4*a124 + k5*a125 + k6*a126 + k7*a127 + k8*a128 + k9*a129 + k10*a1210 + k11*a1211)
    k12 = func(t + h*c12, x)
    x = x_0 + h*(k1*a131 + k4*a134 + k5*a135 + k6*a136 + k7*a137 + k8*a138 + k9*a139 + k10*a1310 + k11*a1311)
    k13 = func(t + h*c13, x)

    x = x_0 + h*(b1*k1 + b6*k6 + b7*k7 + b8*k8 + b9*k9 + b10*k10 + b11*k11 + b12*k12)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat6*k6 + b_hat7*k7 + b_hat8*k8 + b_hat9*k9 + b_hat10*k10 + b_hat11*k11 + b_hat12*k12 + b_hat13*k13)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function Fehlberg78B

function Fehlberg78B(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.055555555555555552
  local a31 = 0.020833333333333332
  local a32 = 0.0625
  local a41 = 0.03125
  local a43 = 0.09375
  local a51 = 0.3125
  local a53 = -1.171875
  local a54 = 1.171875
  local a61 = 0.037499999999999999
  local a64 = 0.1875
  local a65 = 0.14999999999999999
  local a71 = 0.047910137111111112
  local a74 = 0.11224871277777777
  local a75 = -0.025505673777777779
  local a76 = 0.012846823888888888
  local a81 = 0.016917989787292281
  local a84 = 0.3878482784860432
  local a85 = 0.035977369851500331
  local a86 = 0.19697021421566607
  local a87 = -0.17271385234050185
  local a91 = 0.069095753359192297
  local a94 = -0.63424797672885413
  local a95 = -0.16119757522460407
  local a96 = 0.13865030945882525
  local a97 = 0.94092861403575623
  local a98 = 0.21163632648194397
  local a101 = 0.18355699683904539
  local a104 = -2.4687680843155926
  local a105 = -0.29128688781630047
  local a106 = -0.026473020233117376
  local a107 = 2.8478387641928005
  local a108 = 0.28138733146984979
  local a109 = 0.12374489986331466
  local a111 = -1.2154248173958881
  local a114 = 16.672608665945774
  local a115 = 0.91574182841681795
  local a116 = -6.0566058043574706
  local a117 = -16.00357359415618
  local a118 = 14.849303086297663
  local a119 = -13.371575735289849
  local a1110 = 5.134182648179638
  local a121 = 0.25886091643826425
  local a124 = -4.7744857854892047
  local a125 = -0.43509301377703252
  local a126 = -3.0494833320722416
  local a127 = 5.5779200399360995
  local a128 = 6.1558315898610401
  local a129 = -5.0621045867369387
  local a1210 = 2.193926173180679
  local a1211 = 0.13462799865933495
  local a131 = 0.82242759962650747
  local a134 = -11.658673257277664
  local a135 = -0.75762211669093615
  local a136 = 0.71397358815958156
  local a137 = 12.075774986890057
  local a138 = -2.1276591139204029
  local a139 = 1.9901662070489554
  local a1310 = -0.23428647154404028
  local a1311 = 0.17589857770794226

  local b1 = 0.029553213676353499
  local b6 = -0.82860627648779706
  local b7 = 0.31124090005111832
  local b8 = 2.4673451905998869
  local b9 = -2.5469416518419088
  local b10 = 1.4435485836767752
  local b11 = 0.079415595881127288
  local b12 = 0.044444444444444446

  local b_hat1 = 0.041747491141530244
  local b_hat6 = -0.055452328611239311
  local b_hat7 = 0.23931280720118009
  local b_hat8 = 0.70351066940344298
  local b_hat9 = -0.75975961381446089
  local b_hat10 = 0.6605630309222863
  local b_hat11 = 0.15818748251012332
  local b_hat12 = -0.23810953875286281
  local b_hat13 = 0.25

  local c2 = 0.055555555555555552
  local c3 = 0.083333333333333329
  local c4 = 0.125
  local c5 = 0.3125
  local c6 = 0.375
  local c7 = 0.14749999999999999
  local c8 = 0.46500000000000008
  local c9 = 0.56486545138225952
  local c10 = 0.64999999999999969
  local c11 = 0.92465627764050407
  local c12 = 1.0000000000000009
  local c13 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)
  local k9::Vector{Float64} = zeros(Float64, EQN)
  local k10::Vector{Float64} = zeros(Float64, EQN)
  local k11::Vector{Float64} = zeros(Float64, EQN)
  local k12::Vector{Float64} = zeros(Float64, EQN)
  local k13::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 7, 8)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k4*a84 + k5*a85 + k6*a86 + k7*a87)
    k8 = func(t + h*c8, x)
    x = x_0 + h*(k1*a91 + k4*a94 + k5*a95 + k6*a96 + k7*a97 + k8*a98)
    k9 = func(t + h*c9, x)
    x = x_0 + h*(k1*a101 + k4*a104 + k5*a105 + k6*a106 + k7*a107 + k8*a108 + k9*a109)
    k10 = func(t + h*c10, x)
    x = x_0 + h*(k1*a111 + k4*a114 + k5*a115 + k6*a116 + k7*a117 + k8*a118 + k9*a119 + k10*a1110)
    k11 = func(t + h*c11, x)
    x = x_0 + h*(k1*a121 + k4*a124 + k5*a125 + k6*a126 + k7*a127 + k8*a128 + k9*a129 + k10*a1210 + k11*a1211)
    k12 = func(t + h*c12, x)
    x = x_0 + h*(k1*a131 + k4*a134 + k5*a135 + k6*a136 + k7*a137 + k8*a138 + k9*a139 + k10*a1310 + k11*a1311)
    k13 = func(t + h*c13, x)

    x = x_0 + h*(b1*k1 + b6*k6 + b7*k7 + b8*k8 + b9*k9 + b10*k10 + b11*k11 + b12*k12)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat6*k6 + b_hat7*k7 + b_hat8*k8 + b_hat9*k9 + b_hat10*k10 + b_hat11*k11 + b_hat12*k12 + b_hat13*k13)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function Fehlberg78B

"""
Embedded Runge--Kutta with base method's order 7, 
extrapolation order 8 and numbers of stages 13.
With step control and without dense output.

```
DOPRI8(func, A_tol, R_tol, x_0, t_start, t_stop) -> (T, X)
DOPRI8(func, A_tol, R_tol, x_0, t_start, t_stop, last) -> (t, x)
```

# Arguments

* `func::Function`:  right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `A_tol::Float64`: the desired absolute error;
* `R_tol::Float64`: the desired relative error;
* `x_0::Vector{Float64}`: initial value `x_0 = x(t_0)`;
* `t_start::Float64`: starting point of time interval;
* `t_stop::Float64`: final point of time interval;
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` argument:
    * `t::Float64`: time value;
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` argument:
  * `T::Array{Float64, 1}`: the grid points for the numerical integration of 
    the interval `[t_start, t_stop]`; the grid is not equidistant due to the 
    automatic step control;
  * `X::Array{Float64, N}`: all numerical solution points as 2-d array of 
    `Floats64` values with `EQN` columns and `N` rows.
    

"""
function DOPRI8(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.055555555555555552
  local a31 = 0.020833333333333332
  local a32 = 0.0625
  local a41 = 0.03125
  local a43 = 0.09375
  local a51 = 0.3125
  local a53 = -1.171875
  local a54 = 1.171875
  local a61 = 0.037499999999999999
  local a64 = 0.1875
  local a65 = 0.14999999999999999
  local a71 = 0.047910137111111112
  local a74 = 0.11224871277777777
  local a75 = -0.025505673777777779
  local a76 = 0.012846823888888888
  local a81 = 0.016917989787292281
  local a84 = 0.3878482784860432
  local a85 = 0.035977369851500331
  local a86 = 0.19697021421566607
  local a87 = -0.17271385234050185
  local a91 = 0.069095753359192297
  local a94 = -0.63424797672885413
  local a95 = -0.16119757522460407
  local a96 = 0.13865030945882525
  local a97 = 0.94092861403575623
  local a98 = 0.21163632648194397
  local a101 = 0.18355699683904539
  local a104 = -2.4687680843155926
  local a105 = -0.29128688781630047
  local a106 = -0.026473020233117376
  local a107 = 2.8478387641928005
  local a108 = 0.28138733146984979
  local a109 = 0.12374489986331466
  local a111 = -1.2154248173958881
  local a114 = 16.672608665945774
  local a115 = 0.91574182841681795
  local a116 = -6.0566058043574706
  local a117 = -16.00357359415618
  local a118 = 14.849303086297663
  local a119 = -13.371575735289849
  local a1110 = 5.134182648179638
  local a121 = 0.25886091643826425
  local a124 = -4.7744857854892047
  local a125 = -0.43509301377703252
  local a126 = -3.0494833320722416
  local a127 = 5.5779200399360995
  local a128 = 6.1558315898610401
  local a129 = -5.0621045867369387
  local a1210 = 2.193926173180679
  local a1211 = 0.13462799865933495
  local a131 = 0.82242759962650747
  local a134 = -11.658673257277664
  local a135 = -0.75762211669093615
  local a136 = 0.71397358815958156
  local a137 = 12.075774986890057
  local a138 = -2.1276591139204029
  local a139 = 1.9901662070489554
  local a1310 = -0.23428647154404028
  local a1311 = 0.17589857770794226

  local b1 = 0.041747491141530244
  local b6 = -0.055452328611239311
  local b7 = 0.23931280720118009
  local b8 = 0.70351066940344298
  local b9 = -0.75975961381446089
  local b10 = 0.6605630309222863
  local b11 = 0.15818748251012332
  local b12 = -0.23810953875286281
  local b13 = 0.25

  local b_hat1 = 0.029553213676353499
  local b_hat6 = -0.82860627648779706
  local b_hat7 = 0.31124090005111832
  local b_hat8 = 2.4673451905998869
  local b_hat9 = -2.5469416518419088
  local b_hat10 = 1.4435485836767752
  local b_hat11 = 0.079415595881127288
  local b_hat12 = 0.044444444444444446

  local c2 = 0.055555555555555552
  local c3 = 0.083333333333333329
  local c4 = 0.125
  local c5 = 0.3125
  local c6 = 0.375
  local c7 = 0.14749999999999999
  local c8 = 0.46500000000000002
  local c9 = 5.6486545144037823
  local c10 = 0.65000000000000002
  local c11 = 0.9246562776405044
  local c12 = 1
  local c13 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)
  local k9::Vector{Float64} = zeros(Float64, EQN)
  local k10::Vector{Float64} = zeros(Float64, EQN)
  local k11::Vector{Float64} = zeros(Float64, EQN)
  local k12::Vector{Float64} = zeros(Float64, EQN)
  local k13::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 7, 8)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k4*a84 + k5*a85 + k6*a86 + k7*a87)
    k8 = func(t + h*c8, x)
    x = x_0 + h*(k1*a91 + k4*a94 + k5*a95 + k6*a96 + k7*a97 + k8*a98)
    k9 = func(t + h*c9, x)
    x = x_0 + h*(k1*a101 + k4*a104 + k5*a105 + k6*a106 + k7*a107 + k8*a108 + k9*a109)
    k10 = func(t + h*c10, x)
    x = x_0 + h*(k1*a111 + k4*a114 + k5*a115 + k6*a116 + k7*a117 + k8*a118 + k9*a119 + k10*a1110)
    k11 = func(t + h*c11, x)
    x = x_0 + h*(k1*a121 + k4*a124 + k5*a125 + k6*a126 + k7*a127 + k8*a128 + k9*a129 + k10*a1210 + k11*a1211)
    k12 = func(t + h*c12, x)
    x = x_0 + h*(k1*a131 + k4*a134 + k5*a135 + k6*a136 + k7*a137 + k8*a138 + k9*a139 + k10*a1310 + k11*a1311)
    k13 = func(t + h*c13, x)

    x = x_0 + h*(b1*k1 + b6*k6 + b7*k7 + b8*k8 + b9*k9 + b10*k10 + b11*k11 + b12*k12 + b13*k13)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat6*k6 + b_hat7*k7 + b_hat8*k8 + b_hat9*k9 + b_hat10*k10 + b_hat11*k11 + b_hat12*k12)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end

  return(T, transpose(hcat(X...)))
end # function DOPRI8

function DOPRI8(func::Function, A_tol::Float64, R_tol::Float64, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  local EQN = length(x_start)

  local h::Float64
  local h_new::Float64
  local t::Float64
  local err::Float64

  local accepted::Bool
  local last_step::Bool

  local accepted_num::Int64
  local rejected_num::Int64

  local x::Vector{Float64}
  local x_0::Vector{Float64}
  local x_hat::Vector{Float64}

  local a21 = 0.055555555555555552
  local a31 = 0.020833333333333332
  local a32 = 0.0625
  local a41 = 0.03125
  local a43 = 0.09375
  local a51 = 0.3125
  local a53 = -1.171875
  local a54 = 1.171875
  local a61 = 0.037499999999999999
  local a64 = 0.1875
  local a65 = 0.14999999999999999
  local a71 = 0.047910137111111112
  local a74 = 0.11224871277777777
  local a75 = -0.025505673777777779
  local a76 = 0.012846823888888888
  local a81 = 0.016917989787292281
  local a84 = 0.3878482784860432
  local a85 = 0.035977369851500331
  local a86 = 0.19697021421566607
  local a87 = -0.17271385234050185
  local a91 = 0.069095753359192297
  local a94 = -0.63424797672885413
  local a95 = -0.16119757522460407
  local a96 = 0.13865030945882525
  local a97 = 0.94092861403575623
  local a98 = 0.21163632648194397
  local a101 = 0.18355699683904539
  local a104 = -2.4687680843155926
  local a105 = -0.29128688781630047
  local a106 = -0.026473020233117376
  local a107 = 2.8478387641928005
  local a108 = 0.28138733146984979
  local a109 = 0.12374489986331466
  local a111 = -1.2154248173958881
  local a114 = 16.672608665945774
  local a115 = 0.91574182841681795
  local a116 = -6.0566058043574706
  local a117 = -16.00357359415618
  local a118 = 14.849303086297663
  local a119 = -13.371575735289849
  local a1110 = 5.134182648179638
  local a121 = 0.25886091643826425
  local a124 = -4.7744857854892047
  local a125 = -0.43509301377703252
  local a126 = -3.0494833320722416
  local a127 = 5.5779200399360995
  local a128 = 6.1558315898610401
  local a129 = -5.0621045867369387
  local a1210 = 2.193926173180679
  local a1211 = 0.13462799865933495
  local a131 = 0.82242759962650747
  local a134 = -11.658673257277664
  local a135 = -0.75762211669093615
  local a136 = 0.71397358815958156
  local a137 = 12.075774986890057
  local a138 = -2.1276591139204029
  local a139 = 1.9901662070489554
  local a1310 = -0.23428647154404028
  local a1311 = 0.17589857770794226

  local b1 = 0.041747491141530244
  local b6 = -0.055452328611239311
  local b7 = 0.23931280720118009
  local b8 = 0.70351066940344298
  local b9 = -0.75975961381446089
  local b10 = 0.6605630309222863
  local b11 = 0.15818748251012332
  local b12 = -0.23810953875286281
  local b13 = 0.25

  local b_hat1 = 0.029553213676353499
  local b_hat6 = -0.82860627648779706
  local b_hat7 = 0.31124090005111832
  local b_hat8 = 2.4673451905998869
  local b_hat9 = -2.5469416518419088
  local b_hat10 = 1.4435485836767752
  local b_hat11 = 0.079415595881127288
  local b_hat12 = 0.044444444444444446

  local c2 = 0.055555555555555552
  local c3 = 0.083333333333333329
  local c4 = 0.125
  local c5 = 0.3125
  local c6 = 0.375
  local c7 = 0.14749999999999999
  local c8 = 0.46500000000000002
  local c9 = 5.6486545144037823
  local c10 = 0.65000000000000002
  local c11 = 0.9246562776405044
  local c12 = 1
  local c13 = 1

  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  local k8::Vector{Float64} = zeros(Float64, EQN)
  local k9::Vector{Float64} = zeros(Float64, EQN)
  local k10::Vector{Float64} = zeros(Float64, EQN)
  local k11::Vector{Float64} = zeros(Float64, EQN)
  local k12::Vector{Float64} = zeros(Float64, EQN)
  local k13::Vector{Float64} = zeros(Float64, EQN)

  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 7, 8)


  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    x = x_0
    k1 = func(t, x)
    x = x_0 + h*(k1*a21)
    k2 = func(t + h*c2, x)
    x = x_0 + h*(k1*a31 + k2*a32)
    k3 = func(t + h*c3, x)
    x = x_0 + h*(k1*a41 + k3*a43)
    k4 = func(t + h*c4, x)
    x = x_0 + h*(k1*a51 + k3*a53 + k4*a54)
    k5 = func(t + h*c5, x)
    x = x_0 + h*(k1*a61 + k4*a64 + k5*a65)
    k6 = func(t + h*c6, x)
    x = x_0 + h*(k1*a71 + k4*a74 + k5*a75 + k6*a76)
    k7 = func(t + h*c7, x)
    x = x_0 + h*(k1*a81 + k4*a84 + k5*a85 + k6*a86 + k7*a87)
    k8 = func(t + h*c8, x)
    x = x_0 + h*(k1*a91 + k4*a94 + k5*a95 + k6*a96 + k7*a97 + k8*a98)
    k9 = func(t + h*c9, x)
    x = x_0 + h*(k1*a101 + k4*a104 + k5*a105 + k6*a106 + k7*a107 + k8*a108 + k9*a109)
    k10 = func(t + h*c10, x)
    x = x_0 + h*(k1*a111 + k4*a114 + k5*a115 + k6*a116 + k7*a117 + k8*a118 + k9*a119 + k10*a1110)
    k11 = func(t + h*c11, x)
    x = x_0 + h*(k1*a121 + k4*a124 + k5*a125 + k6*a126 + k7*a127 + k8*a128 + k9*a129 + k10*a1210 + k11*a1211)
    k12 = func(t + h*c12, x)
    x = x_0 + h*(k1*a131 + k4*a134 + k5*a135 + k6*a136 + k7*a137 + k8*a138 + k9*a139 + k10*a1310 + k11*a1311)
    k13 = func(t + h*c13, x)

    x = x_0 + h*(b1*k1 + b6*k6 + b7*k7 + b8*k8 + b9*k9 + b10*k10 + b11*k11 + b12*k12 + b13*k13)
    x_hat = x_0 + h*(b_hat1*k1 + b_hat6*k6 + b_hat7*k7 + b_hat8*k8 + b_hat9*k9 + b_hat10*k10 + b_hat11*k11 + b_hat12*k12)

    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

  end

  return (t, x)
end # function DOPRI8

