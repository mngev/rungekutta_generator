module RK

functions = [:RKp2n1,:RKp2n2,:RKp2n3,:RKp3n1,:RKp3n2,:RKp3n3,:RKp3n4,:RKp4n1,:RKp4n2,:RKp4n3,:RKp4n4,:RKp5n1,:RKp5n2,:RKp6n1]

"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp2n1(func, x_0, h, t_start, t_stop) -> (t, x)
RKp2n1(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp2n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1
  
  local b1 = 1/2
  local b2 = 1/2
  
  local c2 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    
    x = x + h*(b1*k1 + b2*k2)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp2n1

function RKp2n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1
  
  local b1 = 1/2
  local b2 = 1/2
  
  local c2 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    
    x = x + h*(b1*k1 + b2*k2)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp2n1
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp2n2(func, x_0, h, t_start, t_stop) -> (t, x)
RKp2n2(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp2n2(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/2
  
  local b2 = 1
  
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t, x + h*(k1*a21))
    
    x = x + h*(b2*k2)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp2n2

function RKp2n2(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/2
  
  local b2 = 1
  
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t, x + h*(k1*a21))
    
    x = x + h*(b2*k2)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp2n2
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp2n3(func, x_0, h, t_start, t_stop) -> (t, x)
RKp2n3(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp2n3(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 2/3
  
  local b1 = 1/4
  local b2 = 3/4
  
  local c2 = 2/3
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    
    x = x + h*(b1*k1 + b2*k2)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp2n3

function RKp2n3(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 2/3
  
  local b1 = 1/4
  local b2 = 3/4
  
  local c2 = 2/3
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    
    x = x + h*(b1*k1 + b2*k2)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp2n3
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp3n1(func, x_0, h, t_start, t_stop) -> (t, x)
RKp3n1(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp3n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 2/3
  local a31 = 1/3
  local a32 = 1/3
  
  local b1 = 1/4
  local b3 = 3/4
  
  local c2 = 2/3
  local c3 = 2/3
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    
    x = x + h*(b1*k1 + b3*k3)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp3n1

function RKp3n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 2/3
  local a31 = 1/3
  local a32 = 1/3
  
  local b1 = 1/4
  local b3 = 3/4
  
  local c2 = 2/3
  local c3 = 2/3
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    
    x = x + h*(b1*k1 + b3*k3)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp3n1
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp3n2(func, x_0, h, t_start, t_stop) -> (t, x)
RKp3n2(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp3n2(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/2
  local a31 = -1
  local a32 = 2
  
  local b1 = 1/6
  local b2 = 2/3
  local b3 = 1/6
  
  local c2 = 1/3
  local c3 = 2/3
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp3n2

function RKp3n2(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/2
  local a31 = -1
  local a32 = 2
  
  local b1 = 1/6
  local b2 = 2/3
  local b3 = 1/6
  
  local c2 = 1/3
  local c3 = 2/3
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp3n2
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp3n3(func, x_0, h, t_start, t_stop) -> (t, x)
RKp3n3(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp3n3(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/3
  local a32 = 2/3
  
  local b1 = 1/4
  local b3 = 3/4
  
  local c2 = 1/3
  local c3 = 2/3
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    
    x = x + h*(b1*k1 + b3*k3)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp3n3

function RKp3n3(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/3
  local a32 = 2/3
  
  local b1 = 1/4
  local b3 = 3/4
  
  local c2 = 1/3
  local c3 = 2/3
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    
    x = x + h*(b1*k1 + b3*k3)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp3n3
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp3n4(func, x_0, h, t_start, t_stop) -> (t, x)
RKp3n4(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp3n4(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/2
  local a32 = 3/4
  
  local b1 = 2/9
  local b2 = 1/3
  local b3 = 4/9
  
  local c2 = 1/2
  local c3 = 3/4
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp3n4

function RKp3n4(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/2
  local a32 = 3/4
  
  local b1 = 2/9
  local b2 = 1/3
  local b3 = 4/9
  
  local c2 = 1/2
  local c3 = 3/4
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp3n4
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp4n1(func, x_0, h, t_start, t_stop) -> (t, x)
RKp4n1(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp4n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/2
  local a32 = 1/2
  local a43 = 1
  
  local b1 = 1/6
  local b2 = 1/3
  local b3 = 1/3
  local b4 = 1/6
  
  local c2 = 1/2
  local c3 = 1/2
  local c4 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    k4 = func(t + h*c4, x + h*(k3*a43))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp4n1

function RKp4n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/2
  local a32 = 1/2
  local a43 = 1
  
  local b1 = 1/6
  local b2 = 1/3
  local b3 = 1/3
  local b4 = 1/6
  
  local c2 = 1/2
  local c3 = 1/2
  local c4 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    k4 = func(t + h*c4, x + h*(k3*a43))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp4n1
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp4n2(func, x_0, h, t_start, t_stop) -> (t, x)
RKp4n2(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp4n2(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/4
  local a32 = 1/2
  local a41 = 1
  local a42 = -2
  local a43 = 2
  
  local b1 = 1/6
  local b3 = 2/3
  local b4 = 1/6
  
  local c2 = 1/4
  local c3 = 1/2
  local c4 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    
    x = x + h*(b1*k1 + b3*k3 + b4*k4)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp4n2

function RKp4n2(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/4
  local a32 = 1/2
  local a41 = 1
  local a42 = -2
  local a43 = 2
  
  local b1 = 1/6
  local b3 = 2/3
  local b4 = 1/6
  
  local c2 = 1/4
  local c3 = 1/2
  local c4 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    
    x = x + h*(b1*k1 + b3*k3 + b4*k4)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp4n2
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp4n3(func, x_0, h, t_start, t_stop) -> (t, x)
RKp4n3(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp4n3(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/3
  local a31 = -1/3
  local a32 = 1
  local a41 = 1
  local a42 = -1
  local a43 = 1
  
  local b1 = 1/8
  local b2 = 3/8
  local b3 = 3/8
  local b4 = 1/8
  
  local c2 = 1/3
  local c3 = 2/3
  local c4 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp4n3

function RKp4n3(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/3
  local a31 = -1/3
  local a32 = 1
  local a41 = 1
  local a42 = -1
  local a43 = 1
  
  local b1 = 1/8
  local b2 = 3/8
  local b3 = 3/8
  local b4 = 1/8
  
  local c2 = 1/3
  local c3 = 2/3
  local c4 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp4n3
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp4n4(func, x_0, h, t_start, t_stop) -> (t, x)
RKp4n4(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp4n4(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/2
  local a32 = 1/2
  local a43 = 1
  
  local b1 = 1/6
  local b2 = 1/3
  local b3 = 1/3
  local b4 = 1/6
  
  local c2 = 1/2
  local c3 = 1/2
  local c4 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    k4 = func(t + h*c4, x + h*(k3*a43))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp4n4

function RKp4n4(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/2
  local a32 = 1/2
  local a43 = 1
  
  local b1 = 1/6
  local b2 = 1/3
  local b3 = 1/3
  local b4 = 1/6
  
  local c2 = 1/2
  local c3 = 1/2
  local c4 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k2*a32))
    k4 = func(t + h*c4, x + h*(k3*a43))
    
    x = x + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp4n4
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp5n1(func, x_0, h, t_start, t_stop) -> (t, x)
RKp5n1(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp5n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/3
  local a31 = 4/25
  local a32 = 6/25
  local a41 = 1/4
  local a42 = -3
  local a43 = 15/4
  local a51 = 2/27
  local a52 = 10/9
  local a53 = -50/81
  local a54 = 8/81
  local a61 = 2/25
  local a62 = 12/25
  local a63 = 2/15
  local a64 = 8/75
  
  local b1 = 23/192
  local b3 = 125/192
  local b5 = -27/64
  local b6 = 125/192
  
  local c2 = 1/3
  local c3 = 2/5
  local c4 = 1
  local c5 = 2/3
  local c6 = 4/5
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54))
    k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64))
    
    x = x + h*(b1*k1 + b3*k3 + b5*k5 + b6*k6)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp5n1

function RKp5n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/3
  local a31 = 4/25
  local a32 = 6/25
  local a41 = 1/4
  local a42 = -3
  local a43 = 15/4
  local a51 = 2/27
  local a52 = 10/9
  local a53 = -50/81
  local a54 = 8/81
  local a61 = 2/25
  local a62 = 12/25
  local a63 = 2/15
  local a64 = 8/75
  
  local b1 = 23/192
  local b3 = 125/192
  local b5 = -27/64
  local b6 = 125/192
  
  local c2 = 1/3
  local c3 = 2/5
  local c4 = 1
  local c5 = 2/3
  local c6 = 4/5
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54))
    k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64))
    
    x = x + h*(b1*k1 + b3*k3 + b5*k5 + b6*k6)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp5n1
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp5n2(func, x_0, h, t_start, t_stop) -> (t, x)
RKp5n2(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp5n2(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/5
  local a31 = 3/40
  local a32 = 9/40
  local a41 = 44/45
  local a42 = -56/15
  local a43 = 32/9
  local a51 = 19372/6561
  local a52 = -25360/2187
  local a53 = 64448/6561
  local a54 = -212/729
  local a61 = 9017/3168
  local a62 = -355/33
  local a63 = 46732/5247
  local a64 = 49/176
  local a65 = -5103/18656
  local a71 = 35/384
  local a73 = 500/1113
  local a74 = 125/192
  local a75 = -2187/6784
  local a76 = 11/84
  
  local b1 = 35/384
  local b3 = 500/1113
  local b4 = 125/192
  local b5 = -2187/6784
  local b6 = 11/84
  
  local c2 = 1/5
  local c3 = 3/10
  local c4 = 4/5
  local c5 = 8/9
  local c6 = 1
  local c7 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54))
    k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65))
    k7 = func(t + h*c7, x + h*(k1*a71 + k3*a73 + k4*a74 + k5*a75 + k6*a76))
    
    x = x + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp5n2

function RKp5n2(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/5
  local a31 = 3/40
  local a32 = 9/40
  local a41 = 44/45
  local a42 = -56/15
  local a43 = 32/9
  local a51 = 19372/6561
  local a52 = -25360/2187
  local a53 = 64448/6561
  local a54 = -212/729
  local a61 = 9017/3168
  local a62 = -355/33
  local a63 = 46732/5247
  local a64 = 49/176
  local a65 = -5103/18656
  local a71 = 35/384
  local a73 = 500/1113
  local a74 = 125/192
  local a75 = -2187/6784
  local a76 = 11/84
  
  local b1 = 35/384
  local b3 = 500/1113
  local b4 = 125/192
  local b5 = -2187/6784
  local b6 = 11/84
  
  local c2 = 1/5
  local c3 = 3/10
  local c4 = 4/5
  local c5 = 8/9
  local c6 = 1
  local c7 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54))
    k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65))
    k7 = func(t + h*c7, x + h*(k1*a71 + k3*a73 + k4*a74 + k5*a75 + k6*a76))
    
    x = x + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp5n2
"""
Runge-Kutta method with constant step size.

The function has two methods:
```
RKp6n1(func, x_0, h, t_start, t_stop) -> (t, x)
RKp6n1(func, x_0, h, t_start, t_stop, last) -> (T, X)
```

# Arguments

* `func::Function`: right side of ODE as function `func(t, x)`
    - `t::Float64`: current time,
    - `x::Vector{Float64`: x(t) value,
    - function shell return `Vector{Float64}`.
* `x_0::Vector{Float64}`: initial value of `x(t)`.
* `h::Float64`: method step size.
* `t_start::Float64`: time interval first point.
* `t_stop::Float64`: time interval last point.
* `last::Bool`: indicates, shell function return only last value (if `true`) or save
                all values in array (if `false`).

# Return values

- With `last` option:
    * `t::Float64`: time value,
    * `x::Vector{Float64}`: approximation of `x(t_stop)`.
- Without `last` option:
    * `T::Vector{Float64}`: times values `[t_start, t_stop]`,
    * `X::Matrix{Float64}`: numerical solution in the form of array with
  `EQN` columns and `N` rows.
"""
function RKp6n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Vector{Float64},Matrix{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  
  local a21 = 1/2
  local a31 = 2/9
  local a32 = 4/9
  local a41 = 7/36
  local a42 = 2/9
  local a43 = -1/12
  local a51 = -35/144
  local a52 = -55/36
  local a53 = 35/48
  local a54 = 15/8
  local a61 = -1/360
  local a62 = -11/36
  local a63 = -1/8
  local a64 = 1/2
  local a65 = 1/10
  local a71 = -41/260
  local a72 = 22/13
  local a73 = 43/156
  local a74 = -118/39
  local a75 = 32/195
  local a76 = 80/39
  
  local b1 = 13/200
  local b3 = 11/40
  local b4 = 11/40
  local b5 = 4/25
  local b6 = 4/25
  local b7 = 13/200
  
  local c2 = 1/2
  local c3 = 2/3
  local c4 = 1/3
  local c5 = 5/6
  local c6 = 1/6
  local c7 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    push!(X, x)
    push!(T, t)
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54))
    k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65))
    k7 = func(t + h*c7, x + h*(k1*a71 + k2*a72 + k3*a73 + k4*a74 + k5*a75 + k6*a76))
    
    x = x + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6 + b7*k7)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return(T, transpose(hcat(X...)))
  
end # RKp6n1

function RKp6n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64, last::Bool)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local a21 = 1/2
  local a31 = 2/9
  local a32 = 4/9
  local a41 = 7/36
  local a42 = 2/9
  local a43 = -1/12
  local a51 = -35/144
  local a52 = -55/36
  local a53 = 35/48
  local a54 = 15/8
  local a61 = -1/360
  local a62 = -11/36
  local a63 = -1/8
  local a64 = 1/2
  local a65 = 1/10
  local a71 = -41/260
  local a72 = 22/13
  local a73 = 43/156
  local a74 = -118/39
  local a75 = 32/195
  local a76 = 80/39
  
  local b1 = 13/200
  local b3 = 11/40
  local b4 = 11/40
  local b5 = 4/25
  local b6 = 4/25
  local b7 = 13/200
  
  local c2 = 1/2
  local c3 = 2/3
  local c4 = 1/3
  local c5 = 5/6
  local c6 = 1/6
  local c7 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54))
    k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65))
    k7 = func(t + h*c7, x + h*(k1*a71 + k2*a72 + k3*a73 + k4*a74 + k5*a75 + k6*a76))
    
    x = x + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6 + b7*k7)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp6n1


end # module RK