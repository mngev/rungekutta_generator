"""
Метод Розенброка
"""
function GRK4A(func::Function, dfunc::Function, Jfunc::Function, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, A_tol::Float64, R_tol::Float64)::Tuple{Vector{Float64},Matrix{Float64}}

  local EQN = length(x_start)
  
  local a21 = 1.1088607594936708
  local a31 = 2.3770852619819602
  local a32 = 0.1850114988898734
  local a41 = 2.3770852619819602
  local a42 = 0.1850114988898734
  
  local a_hat11 = 0.395
  local a_hat21 = -4.920188402397051
  local a_hat31 = 1.055588686049351
  local a_hat32 = 3.351817267668642
  local a_hat41 = 3.846869007047317
  local a_hat42 = 3.4271092412701303
  local a_hat43 = -2.162408848755007
  
  local b1 = 1.8456832404088948
  local b2 = 0.13697968943836447
  local b3 = 0.7129097783279922
  local b4 = 0.6329113924050632
  local b_hat1 = 1.8940019421759449
  local b_hat2 = -0.5101311756702398
  local b_hat3 = 0.9315974443797467
  
  local c2 = 0.438
  local c3 = 0.87
  local c4 = 0.87
  local c_hat1 = 0.395
  local c_hat2 = -0.329672395484
  local c_hat3 = 0.109291965446
  local c_hat4 = 0.43409469625610003
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  local t::Float64
  local x::Vector{Float64}
  
  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 3)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    df = dfunc(t, x_0)
    J = Jfunc(t, x_0)
    
    A = inv(diagm([1.0 / (h * a_hat11) for _ in 1:EQN]) - J)
    
    x = x_0
    k1 = func(t, x) 
    k1 = A * k1
    x = x_0 + k1 * a21
    k2 = func(t + h*c2, x) + (a_hat21 * k1) / h  + c2 * h * df
    k2 = A * k2
    x = x_0 + k1 * a31 + k2 * a32
    k3 = func(t + h*c3, x) + (a_hat31 * k1 + a_hat32 * k2) / h  + c3 * h * df
    k3 = A * k3
    x = x_0 + k1 * a41 + k2 * a42
    k4 = func(t + h*c4, x) + (a_hat41 * k1 + a_hat42 * k2 + a_hat43 * k3) / h  + c4 * h * df
    k4 = A * k4
    
    x = x_0 + b1*k1 + b2*k2 + b3*k3 + b4*k4    
    x_hat = x_0 + b_hat1*k1 + b_hat2*k2 + b_hat3*k3
    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end
  
  return(T, hcat(X...))
  
end # GRK4A

"""
Метод Розенброка
"""
function GRK4T(func::Function, dfunc::Function, Jfunc::Function, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, A_tol::Float64, R_tol::Float64)::Tuple{Vector{Float64},Matrix{Float64}}

  local EQN = length(x_start)
  
  local a21 = 2.0
  local a31 = 4.524708207364943
  local a32 = 4.163528788597403
  local a41 = 4.524708207364943
  local a42 = 4.163528788597403
  
  local a_hat11 = 0.231
  local a_hat21 = -5.071675338768014
  local a_hat31 = -5.64583866184288
  local a_hat32 = 0.15975068467232623
  local a_hat41 = -7.472601017870067
  local a_hat42 = -8.505380858185365
  local a_hat43 = -2.084075136016941
  
  local b1 = 5.621508653236305
  local b2 = 4.6248923883631825
  local b3 = 0.6174772638728525
  local b4 = 1.2826129452683983
  local b_hat1 = 5.570308517016843
  local b_hat2 = 7.698526873491674
  local b_hat3 = -0.2558035379294372
  
  local c2 = 0.462
  local c3 = 0.8802083333333
  local c4 = 0.8802083333333
  local c_hat1 = 0.231
  local c_hat2 = -0.039629667752
  local c_hat3 = 0.5507789395788001
  local c_hat4 = -0.055350984570000025
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  local t::Float64
  local x::Vector{Float64}
  
  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 3)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    df = dfunc(t, x_0)
    J = Jfunc(t, x_0)
    
    A = inv(diagm([1.0 / (h * a_hat11) for _ in 1:EQN]) - J)
    
    x = x_0
    k1 = func(t, x) 
    k1 = A * k1
    x = x_0 + k1 * a21
    k2 = func(t + h*c2, x) + (a_hat21 * k1) / h  + c2 * h * df
    k2 = A * k2
    x = x_0 + k1 * a31 + k2 * a32
    k3 = func(t + h*c3, x) + (a_hat31 * k1 + a_hat32 * k2) / h  + c3 * h * df
    k3 = A * k3
    x = x_0 + k1 * a41 + k2 * a42
    k4 = func(t + h*c4, x) + (a_hat41 * k1 + a_hat42 * k2 + a_hat43 * k3) / h  + c4 * h * df
    k4 = A * k4
    
    x = x_0 + b1*k1 + b2*k2 + b3*k3 + b4*k4    
    x_hat = x_0 + b_hat1*k1 + b_hat2*k2 + b_hat3*k3
    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end
  
  return(T, hcat(X...))
  
end # GRK4T

"""
Метод Розенброка
"""
function ROS3PRL(func::Function, dfunc::Function, Jfunc::Function, x_start::Vector{Float64}, t_start::Float64, t_stop::Float64, A_tol::Float64, R_tol::Float64)::Tuple{Vector{Float64},Matrix{Float64}}

  local EQN = length(x_start)
  
  local a21 = 1.147140180139521
  local a31 = 2.4630707730300534
  local a32 = 1.147140180139521
  local a41 = 2.4630707730300534
  local a42 = 1.147140180139521
  
  local a_hat11 = 0.435866521508459
  local a_hat21 = -2.631861185781065
  local a_hat22 = 0.435866521508459
  local a_hat31 = -2.0384514027343923
  local a_hat32 = 1.855157724001914
  local a_hat33 = 0.435866521508459
  local a_hat41 = -1.8050630466729904
  local a_hat42 = 3.411439279441917
  local a_hat43 = -1.7057196397209584
  local a_hat44 = 0.435866521508459
  
  local b1 = 2.463070773030053
  local b2 = 1.147140180139521
  local b3 = 4.8e-17
  local b4 = 1.0
  local b_hat1 = 2.321182960408909
  local b_hat2 = 0.1793560224446775
  local b_hat3 = 0.06855321332716525
  local b_hat4 = 0.7386849361662245
  
  local c2 = 0.5
  local c3 = 1.0
  local c4 = 1.0
  local c_hat1 = 0.435866521508459
  local c_hat2 = -0.064133478491541
  local c_hat3 = -0.0032561147686687164
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  
  local t::Float64
  local x::Vector{Float64}
  
  x   = copy(x_start)
  x_0 = copy(x_start)
  x_hat = x_start
  t   = t_start

  accepted = true
  last_step = false
  h = step_init(func, t_start, x_start, A_tol, R_tol, 4, 3)

  local X = Vector{Vector{Float64}}()
  local T = Vector{Float64}()
  push!(X, x)
  push!(T, t)

  while !last_step
    # to stop calculation exactly on t_stop point
    if (t + h > t_stop)
      h = t_stop - t
      last_step = true
    end

    # core step
    df = dfunc(t, x_0)
    J = Jfunc(t, x_0)
    
    A = inv(diagm([1.0 / (h * a_hat11) for _ in 1:EQN]) - J)
    
    x = x_0
    k1 = func(t, x) 
    k1 = A * k1
    x = x_0 + k1 * a21
    k2 = func(t + h*c2, x) + (a_hat21 * k1) / h  + c2 * h * df
    k2 = A * k2
    x = x_0 + k1 * a31 + k2 * a32
    k3 = func(t + h*c3, x) + (a_hat31 * k1 + a_hat32 * k2) / h  + c3 * h * df
    k3 = A * k3
    x = x_0 + k1 * a41 + k2 * a42
    k4 = func(t + h*c4, x) + (a_hat41 * k1 + a_hat42 * k2 + a_hat43 * k3) / h  + c4 * h * df
    k4 = A * k4
    
    x = x_0 + b1*k1 + b2*k2 + b3*k3 + b4*k4    
    x_hat = x_0 + b_hat1*k1 + b_hat2*k2 + b_hat3*k3 + b_hat4*k4
    # step control
    (accepted, h_new, err) = step_control(x_0, x, x-x_hat, h, A_tol, R_tol)
    if accepted
      x_0 = x
      t = t + h
      h = h_new
    else
      last_step = false
      # x_0 = x_0
      # t = t
      h = h_new
    end

    if accepted
      push!(X, x)
      push!(T, t)
    end
  end
  
  return(T, hcat(X...))
  
end # ROS3PRL

