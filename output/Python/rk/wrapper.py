# -*- coding: utf-8 -*-
import rk


class RK(object):
    """Класс для поддержания совместимости с предыдущей версиией"""
    def __init__(self, equation_number=None, method_name='RKp4n3'):
        """В конструкторе инициализируем коэффициенты метода"""
        self.method_name = method_name
        self.name = method_name
        try:
            self.method = getattr(rk, self.name)
        except AttributeError:
            print("Нет метода {0}, будем использовать RKp4n3".format(self.name))
            self.method = getattr(rk, 'RKp4n3')

    def integrate(self, func, x_0, time_interval=(0.0, 10.0), h=0.1, params=None):
        """Метод для поддержания совмесимости с предыдущей версией"""
        if params:
            res = self.method(func, x_0, time_interval, h, *params)
        else:
            res = self.method(func, x_0, time_interval, h)
        return res

    def __call__(self, func, x_0, time_interval=(0.0, 10.0), h=0.1, params=None):
        return self.integrate(func, x_0, time_interval, h, params)
