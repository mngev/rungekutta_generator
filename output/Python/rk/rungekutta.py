import numpy as np


def RKp2n1(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp2n1(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1
    b1 = 1/2
    b2 = 1/2
    c2 = 1

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)

        x = x + h*(b1*k1 + b2*k2)

    return times, np.array(xs).T


def RKp2n2(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp2n2(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/2
    b2 = 1

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t, x + h*(k1*a21), *params)

        x = x + h*(b2*k2)

    return times, np.array(xs).T


def RKp2n3(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp2n3(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 2/3
    b1 = 1/4
    b2 = 3/4
    c2 = 2/3

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)

        x = x + h*(b1*k1 + b2*k2)

    return times, np.array(xs).T


def RKp3n1(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp3n1(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 2/3
    a31 = 1/3
    a32 = 1/3
    b1 = 1/4
    b3 = 3/4
    c2 = 2/3
    c3 = 2/3

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32), *params)

        x = x + h*(b1*k1 + b3*k3)

    return times, np.array(xs).T


def RKp3n2(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp3n2(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/2
    a31 = -1
    a32 = 2
    b1 = 1/6
    b2 = 2/3
    b3 = 1/6
    c2 = 1/3
    c3 = 2/3

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32), *params)

        x = x + h*(b1*k1 + b2*k2 + b3*k3)

    return times, np.array(xs).T


def RKp3n3(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp3n3(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/3
    a32 = 2/3
    b1 = 1/4
    b3 = 3/4
    c2 = 1/3
    c3 = 2/3

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k2*a32), *params)

        x = x + h*(b1*k1 + b3*k3)

    return times, np.array(xs).T


def RKp3n4(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp3n4(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/2
    a32 = 3/4
    b1 = 2/9
    b2 = 1/3
    b3 = 4/9
    c2 = 1/2
    c3 = 3/4

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k2*a32), *params)

        x = x + h*(b1*k1 + b2*k2 + b3*k3)

    return times, np.array(xs).T


def RKp4n1(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp4n1(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/2
    a32 = 1/2
    a43 = 1
    b1 = 1/6
    b2 = 1/3
    b3 = 1/3
    b4 = 1/6
    c2 = 1/2
    c3 = 1/2
    c4 = 1

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
        k4 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
        k4 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k2*a32), *params)
        k4 = func(t + h*c4, x + h*(k3*a43), *params)

        x = x + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)

    return times, np.array(xs).T


def RKp4n2(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp4n2(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/4
    a32 = 1/2
    a41 = 1
    a42 = -2
    a43 = 2
    b1 = 1/6
    b3 = 2/3
    b4 = 1/6
    c2 = 1/4
    c3 = 1/2
    c4 = 1

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
        k4 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
        k4 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k2*a32), *params)
        k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43), *params)

        x = x + h*(b1*k1 + b3*k3 + b4*k4)

    return times, np.array(xs).T


def RKp4n3(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp4n3(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/3
    a31 = -1/3
    a32 = 1
    a41 = 1
    a42 = -1
    a43 = 1
    b1 = 1/8
    b2 = 3/8
    b3 = 3/8
    b4 = 1/8
    c2 = 1/3
    c3 = 2/3
    c4 = 1

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
        k4 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
        k4 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32), *params)
        k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43), *params)

        x = x + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)

    return times, np.array(xs).T


def RKp4n4(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp4n4(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/2
    a32 = 1/2
    a43 = 1
    b1 = 1/6
    b2 = 1/3
    b3 = 1/3
    b4 = 1/6
    c2 = 1/2
    c3 = 1/2
    c4 = 1

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
        k4 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
        k4 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k2*a32), *params)
        k4 = func(t + h*c4, x + h*(k3*a43), *params)

        x = x + h*(b1*k1 + b2*k2 + b3*k3 + b4*k4)

    return times, np.array(xs).T


def RKp5n1(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp5n1(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/3
    a31 = 4/25
    a32 = 6/25
    a41 = 1/4
    a42 = -3
    a43 = 15/4
    a51 = 2/27
    a52 = 10/9
    a53 = -50/81
    a54 = 8/81
    a61 = 2/25
    a62 = 12/25
    a63 = 2/15
    a64 = 8/75
    b1 = 23/192
    b3 = 125/192
    b5 = -27/64
    b6 = 125/192
    c2 = 1/3
    c3 = 2/5
    c4 = 1
    c5 = 2/3
    c6 = 4/5

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
        k4 = 0.0
        k5 = 0.0
        k6 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
        k4 = np.zeros(eqn)
        k5 = np.zeros(eqn)
        k6 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32), *params)
        k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43), *params)
        k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54), *params)
        k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64), *params)

        x = x + h*(b1*k1 + b3*k3 + b5*k5 + b6*k6)

    return times, np.array(xs).T


def RKp5n2(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp5n2(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/5
    a31 = 3/40
    a32 = 9/40
    a41 = 44/45
    a42 = -56/15
    a43 = 32/9
    a51 = 19372/6561
    a52 = -25360/2187
    a53 = 64448/6561
    a54 = -212/729
    a61 = 9017/3168
    a62 = -355/33
    a63 = 46732/5247
    a64 = 49/176
    a65 = -5103/18656
    a71 = 35/384
    a73 = 500/1113
    a74 = 125/192
    a75 = -2187/6784
    a76 = 11/84
    b1 = 35/384
    b3 = 500/1113
    b4 = 125/192
    b5 = -2187/6784
    b6 = 11/84
    c2 = 1/5
    c3 = 3/10
    c4 = 4/5
    c5 = 8/9
    c6 = 1
    c7 = 1

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
        k4 = 0.0
        k5 = 0.0
        k6 = 0.0
        k7 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
        k4 = np.zeros(eqn)
        k5 = np.zeros(eqn)
        k6 = np.zeros(eqn)
        k7 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32), *params)
        k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43), *params)
        k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54), *params)
        k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65), *params)
        k7 = func(t + h*c7, x + h*(k1*a71 + k3*a73 + k4*a74 + k5*a75 + k6*a76), *params)

        x = x + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6)

    return times, np.array(xs).T


def RKp6n1(func, x_0: np.ndarray, time_interval: tuple, h: float, *params):
    """Классический метод Рунге-Кутты с постоянным шагом
    RKp6n1(func, x_0, time_interval, h, *params)
    Parameters
    ----------
    func: `function f(t: float, x: np.ndarray, *params) -> ndarray`
        Векторная функция, задающая правую часть уравнения
        $dx(t)/dt = func(t, x, p)$
    x_0 : array of float
        Начальная точка.
    time_interval: tuple of float
        отрезок интегрирования (t_0, t_n)
    h: float
        шаг метода.
    *params:
        дополнительные аргументы функции func

    Returns
    -------
    (T, X): tuple of arrays
        T --- одномерный массив моментов времени,
        X --- многомерный массив решений."""
    x_0 = np.array(x_0)
    eqn = np.size(x_0)
    a21 = 1/2
    a31 = 2/9
    a32 = 4/9
    a41 = 7/36
    a42 = 2/9
    a43 = -1/12
    a51 = -35/144
    a52 = -55/36
    a53 = 35/48
    a54 = 15/8
    a61 = -1/360
    a62 = -11/36
    a63 = -1/8
    a64 = 1/2
    a65 = 1/10
    a71 = -41/260
    a72 = 22/13
    a73 = 43/156
    a74 = -118/39
    a75 = 32/195
    a76 = 80/39
    b1 = 13/200
    b3 = 11/40
    b4 = 11/40
    b5 = 4/25
    b6 = 4/25
    b7 = 13/200
    c2 = 1/2
    c3 = 2/3
    c4 = 1/3
    c5 = 5/6
    c6 = 1/6
    c7 = 1

    if eqn == 1:
        k1 = 0.0
        k2 = 0.0
        k3 = 0.0
        k4 = 0.0
        k5 = 0.0
        k6 = 0.0
        k7 = 0.0
    else:
        k1 = np.zeros(eqn)
        k2 = np.zeros(eqn)
        k3 = np.zeros(eqn)
        k4 = np.zeros(eqn)
        k5 = np.zeros(eqn)
        k6 = np.zeros(eqn)
        k7 = np.zeros(eqn)
    
    times = np.arange(time_interval[0], time_interval[1] + h, h)
    
    x = x_0
    xs = []
    
    for t in times:
        xs.append(x)
        k1 = func(t, x, *params)
        k2 = func(t + h*c2, x + h*(k1*a21), *params)
        k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32), *params)
        k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43), *params)
        k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54), *params)
        k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65), *params)
        k7 = func(t + h*c7, x + h*(k1*a71 + k2*a72 + k3*a73 + k4*a74 + k5*a75 + k6*a76), *params)

        x = x + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6 + b7*k7)

    return times, np.array(xs).T



