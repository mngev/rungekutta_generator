from jinja2 import Environment, FileSystemLoader, select_autoescape
import os.path

from ButcherTable import ButcherTable
from settings import PYTHON, MIN_ORDER


tables = ButcherTable.from_json(PYTHON['RK']['tables'])
env = Environment(loader=FileSystemLoader(PYTHON['RK']['templates']['path']),
                  trim_blocks=True, lstrip_blocks=True)

method = env.get_template(PYTHON['RK']['templates']['method'])
docstring = env.get_template(PYTHON['RK']['templates']['doc'])
python_module = env.get_template(PYTHON['RK']['templates']['module'])

methods_list = []
source_code = []
for table in (tb for tb in tables if tb.p >= MIN_ORDER):
    # print(table.name, end=', ')
    methods_list.append(table.name)
    doc = docstring.render(function_name=table.name)

    rk = method.render(function_name=table.name, stage=table.s,
                       a=table.a, b=table.b, c=table.c, docstring=doc)

    # doc = '"""\n{0:s}\n"""'.format(doc)
    source_code.append('{0}\n\n\n'.format(rk))

python_module = python_module.render(methods_list=methods_list,
                                   source_code="".join(source_code))

with open(PYTHON['RK']['out'], 'w', encoding='utf-8') as file:
    file.write(python_module)
