import numpy as np

from fractions import Fraction
import json as json


def latex_fraction(rational) -> str:
    r = Fraction(rational)
    if r.numerator == 0:
        res = '0'.format(r.numerator, r.denominator)
    elif r.denominator == 1:
        res = '{0:d}'.format(r.numerator, r.denominator)
    else:
        res = '{0:d}/{1:d}'.format(r.numerator, r.denominator)
    return res


latex_frac = np.vectorize(latex_fraction)

frac = np.vectorize(Fraction)


def array_from_list(coefficients):
    """Convert list of strings or lists of stings to fractions"""
    res = np.array(coefficients)
    if res.dtype.type == np.str_:
        res = frac(res)
    else:
        pass
    return res


class ButcherTable(object):
    """Fixed step Runge--Kutta method tableaux"""
    def __init__(self, name, stage, order, eorder=0):
        """
        Construct butcher table for Runge-Kutta method
        :param name: method's name
        :param stage: method's stage
        :param order: method's order
        :param eorder: extrapolation method order (optional, only
        for embedded method)
        """
        self.name = name
        self.description = 'Метод {0}'.format(self.name)
        self.s = stage
        self.p = order
        self.ep = eorder
        self.a = np.zeros(shape=(self.s, self.s))
        self.c = np.zeros(self.s)
        self.b = np.zeros(self.s)
        # for embedded RK method
        if eorder != 0:
            self.b_hat = np.zeros(self.s)

    # def __str__(self):
    #     return self.description

    @classmethod
    def from_json(cls, file_path):
        """Create ButcherTable from JSON file with list of objects"""
        with open(file_path, mode='r', encoding='utf-8') as file:
            data = json.load(file)
        tables = cls.from_dictionary_list(data)
        return tables

    @classmethod
    def from_dictionary_list(cls, dic_list:list):
        """Create ButcherTable from list of python dictionaries"""
        tables = []
        for table in dic_list:
            table = cls.from_dictionary(table)
            tables.append(table)
        return tables

    @classmethod
    def from_dictionary(cls, table:dict):
        """Create ButcherTable from single python dictionary"""
        method = cls(table['name'], table['stage'], table['order'])
        # if embedded Runge--Kutta
        if 'extrapolation_order' in table:
            method.ep = table['extrapolation_order']
            method.b_hat = array_from_list(table['b_hat'])

        # if classical Runge--Kutta
        method.description = table['description']
        method.a = array_from_list(table['a'])
        method.b = array_from_list(table['b'])
        method.c = array_from_list(table['c'])

        return method

    def save_to_json(self, file_path):
        pass

    def latex_table(self):
        """Print Butcher tableaux in LaTeX array format"""
        res = '{0}\n'.format(self.description)
        res += '\\begin{array}{c|' + 'c'*self.s + '}\n'
        C = latex_frac(self.c)
        A = latex_frac(self.a)
        B = latex_frac(self.b)
        if any(v != 0 for v in self.b_hat):
            B_hat = latex_frac(self.b_hat)

        for c, a in zip(C, A):
            res += '{0} & '.format(c)
            res += ' & '.join(['{0}'.format(a) for a in a])
            res += '\\\\\n'
        res += '\\hline\n'
        res += '  & ' + ' & '.join(['{0}'.format(b) for b in B]) + '\\\\\n'
        res += '  & ' + ' & '.join(['{0}'.format(b) for b in B_hat]) + '\n'
        res += '\\end{array}\n'
        return res


class RRKTable(ButcherTable):
    """Rosenbrock-Runge--Kutta method tableaux"""
    def __init__(self, name, stage, order, eorder):
        super(RRKTable, self).__init__(name, stage, order)
        self.ep = eorder
        self.a_hat = np.zeros(shape=(self.s, self.s))
        self.c_hat = np.zeros(self.s)
        self.b_hat = np.zeros(self.s)

    @classmethod
    def from_dictionary(cls, table:dict):
        """Create ButcherTable from single python dictionary"""
        method = cls(table['name'], table['stage'],
                     table['order'], table['extrapolation_order'])
        method.description = table['description']

        method.a = array_from_list(table['a'])
        method.b = array_from_list(table['b'])
        method.c = array_from_list(table['c'])

        method.a_hat = array_from_list(table['a_hat'])
        method.b_hat = array_from_list(table['b_hat'])
        method.c_hat = array_from_list(table['c_hat'])

        return method
