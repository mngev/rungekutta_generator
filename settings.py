from os.path import sep

def settings(language:str)->dict:
  "Установка каталогов для шаблонов и результатов генерации"
  # В случае языка Julia доступны 3 метода
  # RK -- обычные методы Рунге--Кутты
  # ERK -- вложенные методы Рунге--Кутты
  # RRK -- методы Розенброка
  if language.casefold() == "julia":
    lang = 'Julia'
    extention = 'jl'
    methods = ('RK', 'ERK', 'RRK')
  # Для Python доступны только обычные методы Рунге-Кутты
  if language.casefold() == "python":
    lang = 'Python'
    extention = 'py'
    methods = ('RK',)
  res = {}
  for method in methods:
    res[method] = {
      # каталог с таблицами
      "tables": f"tables{sep}{method.casefold()}_tables.json",
      "templates": {
        # Путь к файлам шаблонов (для Jinja2 нужен именно каталог)
        "path": f"templates{sep}{lang}{sep}{method.casefold()}",
        # шаблон модуля
        "module": f"module.{extention}",
        # шаблон документации к функциям
        "doc": f"doc.md",
        # шаблон самих функций
        "method": f"method.{extention}",
      },
      # Сгенерированный файл
      "out" : f"output{sep}{lang}{sep}{method.casefold()}{sep}{method.casefold()}.{extention}"
    }
  return res


JULIA = settings("Julia")
PYTHON = settings("Python")

MIN_ORDER = 1

if __name__ == "__main__":
  import pprint
  pp = pprint.PrettyPrinter(indent=1)
  pp.pprint(JULIA)
  pp.pprint(PYTHON)