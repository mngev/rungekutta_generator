from jinja2 import Environment, FileSystemLoader, select_autoescape
import os.path

from ButcherTable import ButcherTable
from settings import JULIA, MIN_ORDER

tables = ButcherTable.from_json(JULIA['ERK']['tables'])


env = Environment(loader=FileSystemLoader(JULIA['ERK']['templates']['path']),
                  trim_blocks=True, lstrip_blocks=True)

method = env.get_template(JULIA['ERK']['templates']['method'])
doc_string = env.get_template(JULIA['ERK']['templates']['doc'])
julia_module = env.get_template(JULIA['ERK']['templates']['module'])

methods_list = []
source_base = []
source_with_info = []

for table in (tb for tb in tables if tb.p >= MIN_ORDER):
    methods_list.append(table.name)
    # Параметр only_last включает/выключает сохранение вычисленных
    # значений на каждом шаге
    # Параметр with_info включает сохранение дополнительной информации
    # на каждом шаге
    doc1 = doc_string.render(
        function_name=table.name,
        stage=table.s,
        order=table.p,
        eoder=table.ep,
        only_last=False,
        with_info=False)

    rk1 = method.render(
        function_name=table.name,
        stage=table.s,
        order=table.p,
        eoder=table.ep,
        a=table.a,
        b=table.b,
        b_hat=table.b_hat,
        c=table.c,
        only_last=False,
        with_info=False)

    rk2 = method.render(
        function_name=table.name,
        stage=table.s,
        order=table.p,
        eoder=table.ep,
        a=table.a,
        b=table.b,
        b_hat=table.b_hat,
        c=table.c,
        only_last=True,
        with_info=False)

    doc3 = doc_string.render(
        function_name=table.name,
        stage=table.s,
        order=table.p,
        eoder=table.ep,
        only_last=False,
        with_info=True)

    rk3 = method.render(
        function_name=table.name,
        stage=table.s,
        order=table.p,
        eoder=table.ep,
        a=table.a,
        b=table.b,
        b_hat=table.b_hat,
        c=table.c,
        only_last=False,
        with_info=True)

    doc1 = '"""\n{0:s}\n"""'.format(doc1)
    doc3 = '"""\n{0:s}\n"""'.format(doc3)
    res = '{0}\n{1}\n\n{2}\n\n'.format(doc1, rk1, rk2)
    source_base.append(res)
    res = '{0}\n{1}\n\n'.format(doc3, rk3)
    source_with_info.append(res)

julia_module = julia_module.render(methods_list=methods_list)
# Чтобы не мешало расширение файла, пропустим 3 последних символа
with open(JULIA['ERK']['out'][:-3]+'_base.jl', 'w', encoding='utf-8') as file1,\
     open(JULIA['ERK']['out'][:-3]+'_info.jl', 'w', encoding='utf-8') as file2,\
     open(JULIA['ERK']['out'], 'w', encoding='utf-8') as file3:
    
    file1.write("".join(source_base))
    file2.write("".join(source_with_info))
    file3.write(julia_module)
